     <div class="container">
         <div class="shadow-effect main-content">
             <div class="row-fluid pattern-background">
                 <div class="span9 offset1">
                     <h1>Frequently Asked Questions</h1>
                     <h4>Q - I want to be a Locator! How do I join Troy Dunn’s team?</h4>
                     <p>It’s easy! Click on “Join Team Troy” and fill out the form completely then submit it. That’s it! Your Little Locator status will be activated and you will start receiving cases! Cases are only sent when something in your profile is a data match to something in a case, so be patient. You might get a case moments after signing up or it may be awhile before your first TroyAlert pops in! TIP- the more details you provide in your profile, the higher number of cases you will be sent to work on!</p>
                     <h4>Q - Does it cost anything to join Troy Dunn’s team and become a Little Locator?</h4>
                     <p>Absolutely NOT! Troy Dunn is building the largest team of volunteer Little Locators in the world so he can help more people by solving more cases! No costs involved so join Team Troy right now!</p>
                     <h4>Q - I want to become a Little Locator but am concerned about the privacy of my profile information. Where does that information go?</h4>
                     <p>Troy Dunn and his team have been protecting people’s very confidential information for almost 25 years without a single breach or loss of information. All of your information is encrypted and secured in a digital data vault which can only be accessed by a select number of Troy Dunn’s internal research team for selecting Little Locators to send cases to. And of course, we do not ever sell or rent ANY data so your information is totally secure.</p>
                     <h4>Q - I would love to have a TroyAlert sent out to help ME find a long lost loved one. Is that possible?</h4>
                     <p>Absolutely! Click here for directions on how you can submit your own request for a TroyAlert! We will see if we can help you solve your own mystery!</p>
                     <h4>Q - I want to help Troy solve cases and can’t wait to become a Little Locator but I don’t want my name mentioned on TV. Can I help without being on TV?</h4>
                     <p>No problem! Troy never mentions the identities of any Little Locators on TV without their permission. So jump in and start locating! Mum’s the word on your identity!</p>
                     <h4>Q - I have a technical question about the website or the app. Where can I get help with that?</h4>
                     <p>That’s easy! Just fill out the <a href="contact">Contact Form</a> and select Technical Issue as the subject.</p>
                 </div>
             </div>
         </div>
     </div>

