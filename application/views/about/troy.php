     <div class="container">
         <div class="shadow-effect main-content">
             <div class="row-fluid pattern-background">
                 <div class="span9 offset1">
                     <h1>About</h1>
                     <img src="../assets/images/image-of-Troy-Dunn.jpg" class="pull-right">
                     <p>For many years, Troy Dunn (aka 'The Locator') has become known for reuniting tens of thousands of people all around the world with long lost friends and family. Many of his most memorable reunions have been featured on TV. This success has increased the number of people reaching out to his team for help to thousands every day!</p>
                     <p>Burdened with the guilt of not being able to help everyone who so desperately needs help, and not being able to solve some of the most difficult, most urgent cases in the world, a solution was needed quickly! Troy sat down with his team of brilliant technology developers and programmers to create a solution. Troy Alert is that solution!</p>
                     <p>Troy Alert empowers Troy Dunn and his team with a virtual army of 'Little Locators' that span the world and are able to solve difficult cases in record time! No training required to become a 'Little Locator!' Simply fill out the "Join Team Troy" form and you will instantly be added to the team. Then just wait for a Troy Alert to pop up on your device or in your email with your first case!</p>
                     <p>Join Team Troy right now! More 'Little Locators' needed! Cases are waiting and new cases are being taken every hour!</p>
                 </div>
             </div>
         </div>
     </div>


