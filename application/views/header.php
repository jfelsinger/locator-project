<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Troy Alert</title>

    <!-- google webfonts -->
    <link href='http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic,700italic|Droid+Sans:400,700|Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic|PT+Sans:400,700,400italic,700italic|Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800|Nunito:400,700' rel='stylesheet' type='text/css'>

    <link href="/assets/css/jquery-ui.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap-responsive.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/camera.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/custom.css" rel="stylesheet" type="text/css">
    <!--HTML5 shim for IE backwards compatibility-->
    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/truck/html5.js"></script>
    <![endif]-->
    <!--
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script type='text/javascript' src='/assets/js/jquery.mobile.customized.min.js'></script>
    <script type='text/javascript' src='/assets/js/jquery.easing.1.3.js'></script>   
    <script type='text/javascript' src='/assets/js/camera.js'></script> 

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js" type="text/javascript"></script>
    <script type='text/javascript' src='/assets/js/autocomplete.js'></script>

    <script>
        $(function(){

            $('#camera_wrap').camera({
                height: '41%',
                loader: 'bar',
                pagination: true,
                thumbnails: false,
                time: 2000,
                fx: 'simpleFade',
            });
        });
    </script>


    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-38039417-1']);
        _gaq.push(['_trackPageview']);

        (function() {
         var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
         ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
         var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
         })();

    </script>
</head>

<body>

    <!-- beginning of header-->
    <header>
        <div class="container">
            <div class="row-fluid">
                <div class="span6 inline"> 
                    <a href="<?= base_url(); ?>index">
                        <img src="/assets/images/Troy-Alert-Small.png" alt="Troy Alert Logo">
                    </a> 
                </div>
                <div class="headerWrapper span6">
                    <nav class="navbar navbar-inverse">
                        <div class="navbar-inner"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> 
                                <span class="icon-bar"></span> 
                                <span class="icon-bar"></span> 
                                <span class="icon-bar"></span></a> 
                            <div class="nav-collapse">
                                <ul class="nav pull-right">
                                    <li class="active"><a href="/index">Home</a></li>
                                    <li><a href="/about/troy">About</a></li>
                                    <li><a href="/faq">FAQ</a></li>
                                    <li><a href="/contact">Contact</a></li>
                                    <li><a href="/login">Login</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- end of header--> 



