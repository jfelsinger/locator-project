     <!-- start of slider -->
     <div class="container">
         <div class="camera_wrap" id="camera_wrap">
             <div data-src="assets/images/slider1.jpg">
                 <div class="caption caption-left moveFromRight"><a href="#"><h3>Looking for</h3><h3>a lost</h3><h3>loved one?</h3><h3>Sign up as a</h3><h2>seeker</h2></a></div>
             </div>        	
             <div data-src="assets/images/bridge.jpg">
                 <div class="caption moveFromLeft"><a href="#"><h2>Troy Alert</h2><h3>The search app for the 21st century.</h3></a></div>
             </div>
             <div data-src="assets/images/road.jpg">
                 <div class="caption fadeIn"><a href="#"><h3>Help us find people.</h3><h3>Become a </h3><h2>locator</h2></a></div>
             </div>
             <div data-src="assets/images/sea.jpg">
                 <div class="caption moveFromLeft"><a href="#"><h2>Troy Alert</h2><h3>Search in the 21st century.</h3></a></div>
             </div>
         </div>  <!-- camera wrap -->
     </div>  <!-- container -->

     <!-- end of slider -->

     <!--social content-->
     <div class="socialContent">
         <div class="container">
             <div class="shadow-effect box-container">
                 <div class="row-fluid">
                     <div class="span4 leftBox">
                        <a href="about"><img src="assets/images/help-icon.png" class="icon"></a>
                         <h3>What is Troy Alert?</h3>
                         <p>Troy Alert is a new app brought to you by Troy Dunn. For many years, Troy Dunn has become known
                         for reuniting tens of thousands of people all around the world. Troy Alert empowers Troy Dunn and his team with a virtual army of locators. Simply fill out the Locator form and you will be instantly added to the team! Then just wait for a Troy Alert to pop up on your device or in your email with your first case!</p>
                         
                     </div> <!-- span4 -->
                     
                     <div class="span4 middleBox">
                        <a href="register"><img src="assets/images/pencil-icon.png" class="icon"></a>
                         <h3>Sign Up</h3>
                         <div class="media"> 
                             <div class="media-body">                                 
                                 <p>All you have to do is create an online profile or just use your Facebook information! We will keep your information safe and secure.</p>
                             </div><!-- media body -->

                             <div class="media-body">
                                 <h4 class="media-heading">Need Help?</h4>
                                 <p>Check out our very informative <a href="faq">FAQ</a> page to get all your questions answered or go to our <a href="contact">Contact</a> page to see how to get a hold of us.</p>
                             </div><!-- media body -->

                         </div>  <!-- media -->
                     </div>  <!-- span4 -->
                     <div class="span4 rightBox">
                        <img src="assets/images/magnifier-icon.png" class="icon">
                         <h3>Find Someone</h3>
                         <div class="media">
                            <p>Just fill out the <a href="register/seeker">Seeker</a> application to request that a Troy Alert be sent out!</p>
                             
                             <div class="media-body">
                                 
                             </div><!--media-body-->
                         </div><!-- media-->
                         <h3>Follow Us</h3>
                         <a href="facebook.com"> <img src="assets/images/logos/facebook-logo-64X64.png" alt="Facebook Logo-64X64"></a>
                         <a href="#"><img src="assets/images/logos/twitter-logo-square-64X64.png" alt="Twitter Logo"></a>
                         <a href="#"> <img src="assets/images/logos/linkedin-logo-64X64.png" alt="Linkedin logo"></a>
                         <!--<a class="pull-left" href="#"> <img src="assets/images/logos/YouTubeLogo1.png" alt="You Tube Logo"></a>-->
                     </div>  <!-- span4 -->
                 </div> <!-- row fluid -->
             </div> <!-- shadow effect -->
         </div> <!-- container -->
     </div> <!-- social content -->
     <!-- end of social content--> 