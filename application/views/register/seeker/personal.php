    <div class="container main-content">
        <div class="row-fluid">

            
            <div class="span12 patch-well">

                <div class="progress-bar center">
                        <img src="/assets/images/ProgressBar.png" alt="progress bar icon">
                </div>

                <h1>Personal Profile</h1>

                <p>Please provide us with information about yourself, the more we know about you the easier it will be to find your target.</p>

                <div>&nbsp; &nbsp; Use my info from:

                     <button onclick="start();" class="btn btn-medium btn-info">facebook</button></div>

                <div class="span6">

                    <form id="locPersonal" class="locPersonal" name="locPersonal" method="post">
                        <fieldset>
                            <legend>Name Information</legend>
                            <label for="personFName" class="required">First Name:</label>
                            <input type="text" value="<?= set_value('personFName'); ?>" placeholder="John" id="personFName" name="personFName">

                            <label for="personMName">Middle Name:</label>
                            <input type="text" value="<?= set_value('personMName'); ?>" placeholder="Allen" id="personMName" name="personMName">

                            <label for="personLName" class="required">Last Name:</label>
                            <input type="text" value="<?= set_value('personLName'); ?>" placeholder="Doe" id="personLName" name="personLName">

                            <label for="personSuffix">Suffix (Jr, Sr., etc.):</label>
                            <input type="text" value="<?= set_value('personSuffix'); ?>" id="personSuffix" name="personSuffix" >

                            <label for="personMaName">Maiden Name (if applies):</label>
                            <input type="text" value="<?= set_value('personMaName'); ?>" id="personMaName" name="personMaName" placeholder="">
                        </fieldset>
                        <fieldset>
                            <legend>Phone Information</legend>

                            <a href="#">Why is this important?</a>

                            <label for="cell">Cell Phone:</label>
                            <input type="tel" value="<?= set_value('cell'); ?>" placeholder="000-000-0000" id="cell" name="cell"/>

                            <label for="home">Home Phone:</label>
                            <input type="tel" value="<?= set_value('home'); ?>" placeholder="000-000-0000" id="home" name="home"/>

                            <label for="work">Work Number:</label>
                            <input type="tel" value="<?= set_value('work'); ?>" placeholder="000-000-0000" id="work" name="work"/>

                            <label for="fax">Fax Number:</label>
                            <input type="tel" value="<?= set_value('fax'); ?>" placeholder="000-000-0000" id="fax" name="fax"/>
                        </fieldset>


                    </div>
                    <div class="span5">


                        <!-- radio buttons for gender -->

                        <fieldset>
                            <legend>Gender</legend>
                            <label class="radio">
                                <input type="radio" id="m" name="personGender" title="male" value="male" <?= set_radio('personGender', 'male'); ?>>Male
                            </label>
                            <label class="radio">
                                <input type="radio" id="f" name="personGender" title="female" value="female" <?= set_radio('personGender', 'female'); ?>>Female
                            </label>
                        </fieldset>

                        <!-- radio buttons for race -->
                        <fieldset>
                            <legend>Race</legend>

                            <?php foreach($races as $race) { ?>
                            <label class="radio">
                                <input type="radio" id="<?= $race['value']; ?>" name="personRace" title="<?= $race['value']; ?>" value="<?= $race['value']; ?>" <?= set_radio('personRace', $race['value']); ?>>
                                <?= $race['name']; ?></label>
                            <?php } ?>
                        </fieldset>

                        <input type="submit" name="submit" class="btn btn-medium btn-warning" value="Save &amp; Continue"/>
                        <input id="btnSaveExit" type="reset" class="btn btn-medium btn-info" value="Save &amp; Exit"/>
                        <div class="tipContent">You may login later at any time &amp; finish your profile!</div>

                                <!-- FIX FUNCTIONALITY ON SAVE & EXIT BUTTON -->
                    </form>
                </div> <!--span5-->

            </div> <!--span9-->
        </div> <!--row fluid-->
    </div> <!-- container main content-->

    <!-- end of form-->

