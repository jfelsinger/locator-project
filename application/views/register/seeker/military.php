    <div class="container main-content">
        <div class="row-fluid">

            <div class="span12 patch-well">

                <h2>Military</h2>

                <div class="span6">
                    <form id="locEducation" method="post"> 
                        <fieldset>
                            <legend>Service Information</legend>

                            <label for="placeName">Branch:</label>
                            <select name="placeName" id="placeName">
                                <option value="0" <?= set_select('placeName', 0); ?>>Select One...</option>
                                <option value="1" <?= set_select('placeName', 1); ?>>Army</option>
                                <option value="2" <?= set_select('placeName', 2); ?>>Navy</option>
                                <option value="3" <?= set_select('placeName', 3); ?>>Air Force</option>
                                <option value="4" <?= set_select('placeName', 4); ?>>Marines</option>
                                <option value="5" <?= set_select('placeName', 5); ?>>Coast Guard</option>
                            </select>
                        </fieldset>  
                    </div>

                    <div class="span5">       
                        <fieldset>
                            <legend>Dates Served</legend>

                            <label for="from">From:</label>
                            <input type="text" id="from" name="dateFrom" class="datepicker" placeholder="01/01/1970" <?= set_value('dateFrom'); ?>>

                            <label for="to">To:</label>
                            <input type="text" id="to" name="dateTo" class="datepicker" placeholder="01/01/1970" <?= set_value('dateTo'); ?>>

                        </fieldset>       

                            <input type="submit" name="submit" class="btn btn-medium btn-warning" value="Submit">

                            <!-- Reset button, not generally needed, might be better to give space for the skip button
                            <input type="reset" class="btn btn-medium btn-info" value="Reset">
                            -->

                            <?php if (isset($places) && count($places)) { ?>
                            <a href="<?= $skipUrl; ?>" class="btn btn-medium btn-info">Continue</a>
                            <?php } else { ?>
                            <a href="<?= $skipUrl; ?>" class="btn btn-medium btn-info">Skip</a>
                            <?php } ?>

                    </form>
                </div>


            </div> <!-- span9 -->
        </div> <!-- row fluid -->
    </div> <!-- container main-content -->

    <!-- end of form-->

