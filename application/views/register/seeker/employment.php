    <div class="container main-content">
        <div class="row-fluid">

            <div class="span12 patch-well">
                <div class="row-fluid">

                <div class="progress-bar center">
                            <img src="/assets/images/ProgressBar.png" alt="progress bar icon">
                    </div>    

                    <h1>Work History</h1>

                    <div> <!-- Table to be displayed if adding another place -->
                        <?php if (isset($places)) foreach($places as $place) { ?>
                        <table>
                            <td><?= ($place->getName()) ? $place->getName() : $place->getAddress1(); ?></td>
                            <td><?= $place->getFromDate() . ' - ' . $place->getToDate(); ?></td>
                        </table>
                        <?php } ?>
                    </div>

                <div class="span6">
                    <form id="locWork" class="locWork" name="locWork" method="post">
                        <fieldset>
                            <legend>Work Background</legend>
                            <label for="associationType">How are you associated with this workplace?</label>
                            <select  name="associationType" id="associationType">
                                <option value="7" <?= set_select('associationType', 7); ?>>Owner</option>
                                <option value="2" <?= set_select('associationType', 2); ?>>Employee</option>
                                <option value="0" <?= set_select('associationType', 0); ?>>Other</option>
                            </select>

                            <label for="placeType">What type of business is this?</label>
                            <select name="placeType" id="placeType">
                                <option value="2" <?= set_select('associationType', 2); ?>>Restaurant</option>
                                <option value="6" <?= set_select('associationType', 6); ?>>General Business</option>
                                <option value="11" <?= set_select('associationType', 11); ?>>Hospital</option>
                                <option value="12" <?= set_select('associationType', 12); ?>>Government Office</option>
                                <option value="13" <?= set_select('associationType', 13); ?>>Landmark</option>
                                <option value="14" <?= set_select('associationType', 14); ?>>Farm</option>
                                <option value="15" <?= set_select('associationType', 15); ?>>Attraction</option>
                                <option value="0" <?= set_select('associationType', 0); ?>>Other</option>
                            </select>

                            <label for="placeName">Name of work place:</label>
                            <input type="text" name="placeName" id="placeName" <?= set_value('placeName'); ?>>
                        </fieldset>


                        <label for="placeAddress1">Address:</label>
                        <input type="text" id="placeAddress1" name="placeAddress1" <?= set_value('placeAddress1'); ?>>

                        <label for="placeAddress2">Address Line 2:</label>
                        <input type="text" id="placeAddress2" name="placeAddress2" <?= set_value('placeAddress2'); ?>>

                        <label for="placeCity">City:</label>
                        <input type="text" id="placeCity" name="placeCity" <?= set_value('placeCity'); ?>>

                        <label for="placeRegion">State/Region:</label>
                        <input type="text" id="placeRegion" name="placeRegion" <?= set_value('placeRegion'); ?>>

                        <label for="placeZipcode">Zip Code:</label>
                        <input type="text" id="placeZipcode" name="placeZipcode" <?= set_value('placeZipcode'); ?>>

                        <label for="placeCountry">Country:</label>
                        <select name="placeCountry" id="placeCountry">
                            <?php foreach ($countries as $country) { ?>
                            <option value="<?= $country['value']; ?>" <?= set_select('placeCountry', $country['value']); ?> ><?= $country['name']; ?></option>
                            <?php } ?>
                        </select>

                    </div>
                    <div class="span5">

                        <fieldset>
                            <legend>Dates Worked</legend> 
                            <label for="from">From:</label>
                            <input type="text" id="from" name="dateFrom" class="datepicker" placeholder="01/01/1970" <?= set_value('dateFrom'); ?>>
                            <label for="to">To:</label>
                            <input type="text" id="to" name="dateTo" class="datepicker" placeholder="01/01/1970" <?= set_value('dateTo'); ?>>
                        </fieldset>

                        <?php if (isset($places) && count($places)) { ?>
                        <input type="submit" name="submit" class="btn btn-medium btn-warning" value="Add Another Place">
                        <?php } else { ?>
                        <input type="submit" name="submit" class="btn btn-medium btn-warning" value="Save &amp; Continue">
                        <?php } ?>

                        <!-- Reset button, not generally needed, might be better to give space for the skip button
                        <input type="reset" class="btn btn-medium btn-info" value="Reset">
                        -->

                        <?php if (isset($places) && count($places)) { ?>
                        <a href="<?= $skipUrl; ?>" class="btn btn-medium btn-info">Continue</a>
                        <?php } else { ?>
                        <a href="<?= $skipUrl; ?>" class="btn btn-medium btn-info">Skip</a>
                        <?php } ?>
                    </form>

                    </div> <!-- span5 -->
                </div> <!-- row-fluid -->
            </div> <!-- span9 -->
        </div> <!-- row-fluid -->
    </div> <!-- container main-content -->
    <!-- end of form-->

