    <div class="container main-content">
        <div class="row-fluid">

            
            <div class="span12 patch-well">
                <div class="row-fluid">

                    <div class="progress-bar center">
                            <img src="/assets/images/ProgressBar.png" alt="progress bar icon">
                    </div> 

                    <h1>Sign Up as a Seeker</h1>

                    <div class="span6">
                        <form method="post">
                            <fieldset>
                                <legend>Login Information</legend>
                                <p>Looking to find someone? These first steps are required so that you can login and check the status on your case.</p>

                                <label for="username" class="required">Create a Username <a href="#" class="icon-question-sign" title="Why is this required?"></a></label>
                                <?= form_error('username'); ?>
                                <input required type="text" placeholder="UserName123" value="<?= set_value('username'); ?>" id="username" name="username">

                                <label for="usersEmail" class="required">Your Email Address:</label>
                                <?= form_error('email'); ?>
                                <input required type="email" placeholder="abc@google.com" value="<?= set_value('email'); ?>" id="usersEmail" name="email">

                                <label for="birthDate" class="birthDate required">Birth Date</label>
                                <?= form_error('birthdate'); ?>
                                <input required type="text" name="birthdate" value="<?= set_value('birthdate'); ?>" id="datepicker" placeholder="01/01/1970" />

                                <label for="password" class="required">Create your password</label>
                                <?= form_error('password'); ?>
                                <input required type="password" value="<?= set_value('password'); ?>" id="password" name="password">

                                <label for="password2" class="required">Re-enter your password</label>
                                <?= form_error('password2'); ?>
                                <input required type="password" value="<?= set_value('password2'); ?>" id="password2" name="password2">
                            </fieldset>

                            <input type="submit" name="submit" class="btn btn-medium btn-warning" value="Submit">
                            <input type="reset" class="btn btn-medium btn-info" value="Reset">

                        </form>    
                    </div> <!-- span6 -->
                </div> <!-- row-fluid -->
            </div> <!-- span9 -->
        </div> <!-- row-fluid -->
    </div> <!-- container -->

    <!-- end of form-->

