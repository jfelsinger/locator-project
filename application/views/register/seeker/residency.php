    <div class="container main-content">
        <div class="row-fluid">

            <div class="span12 patch-well">

                <div class="progress-bar center">
                        <img src="/assets/images/ProgressBar.png" alt="progress bar icon">
                </div>    

                <h1>Residential History</h1>  


                    <div> <!-- Table to be displayed if adding another place -->
                        <?php if (isset($places)) foreach($places as $place) { ?>
                        <table>
                            <td><?= ($place->getName()) ? $place->getName() : $place->getAddress1(); ?></td>
                            <td><?= $place->getFromDate() . ' - ' . $place->getToDate(); ?></td>
                        </table>
                        <?php } ?>
                    </div>

                <div class="span6">
                    <form id="locResidence" class="locResidence" name="locResidence" method="post">
                        <fieldset>
                            <legend>Places You Have Lived</legend>

                            <label for="placeName">Name: (optional)</label>
                            <input type="text" name="placeName" id="placeName" placeholder="i.e. name of development" <?= set_value('placeName'); ?>>

                            <label for="associationType">How are you associated with this location?</label>
                            <select  name="associationType" id="associationType">
                                <option value="4">Current Resident</option>
                                <option value="0">Past Resident</option>
                                <option value="6">Place of Birth</option>
                            </select>

                            <label for="placeAddress1">Address:</label>
                            <input type="text" id="placeAddress1" name="placeAddress1" <?= set_value('placeAddress1'); ?>>

                            <label for="placeAddress2">Address Line 2:</label>
                            <input type="text" id="placeAddress2" name="placeAddress2" <?= set_value('placeAddress2'); ?>>

                            <label for="placeCity">City:</label>
                            <input type="text" id="placeCity" name="placeCity" <?= set_value('placeCity'); ?>>

                            <label for="placeRegion">State/Region:</label>
                            <input type="text" id="placeRegion" name="placeRegion" <?= set_value('placeRegion'); ?>>

                            <label for="placeZipcode">Zip Code:</label>
                            <input type="text" id="placeZipcode" name="placeZipcode" <?= set_value('placeZipcode'); ?>>

                            <label for="placeCountry">Country:</label>
                            <select name="placeCountry" id="placeCountry">
                                <?php foreach ($countries as $country) { ?>
                                <option value="<?= $country['value']; ?>" <?= set_select('placeCountry', $country['value']); ?> ><?= $country['name']; ?></option>
                                <?php } ?>
                            </select>     
                        </fieldset>
                    </div>

                    <div class="span5">
                        <fieldset>
                            <legend>Date Of Residency</legend>
                            <label for="from">From:</label>
                            <input type="text" id="from" name="dateFrom" class="datepicker" placeholder="01/01/1970" <?= set_value('dateFrom'); ?>>
                            <label for="to">To:</label>
                            <input type="text" id="to" name="dateTo" class="datepicker" placeholder="01/01/1970" <?= set_value('dateTo'); ?>>
                        </fieldset>        

                        <?php if (isset($places) && count($places)) { ?>
                        <input type="submit" name="submit" class="btn btn-medium btn-warning" value="Add Another Place">
                        <?php } else { ?>
                        <input type="submit" name="submit" class="btn btn-medium btn-warning" value="Save &amp; Continue">
                        <?php } ?>

                        <!-- Reset button, not generally needed, might be better to give space for the skip button
                        <input type="reset" class="btn btn-medium btn-info" value="Reset">
                        -->

                        <?php if (isset($places) && count($places)) { ?>
                        <a href="<?= $skipUrl; ?>" class="btn btn-medium btn-info">Continue</a>
                        <?php } else { ?>
                        <a href="<?= $skipUrl; ?>" class="btn btn-medium btn-info">Skip</a>
                        <?php } ?>
                    </form>

                </div> <!-- span5 -->

            </div> <!-- span9 -->
        </div> <!-- row-fluid -->
    </div> <!-- container main-content -->

    <!-- end of form--> 

