<?php
    if (!isset($progress))
        $progress = "0";

    if (!isset($active))
        $active = 0;
?>
            <!--Side Navigation-->  

            <div class="span3">
                <div class="well sidebar-nav">
                    <div class="accordion" id="accordion2">
                        <div class="accordion-group">
                            <div class="accordion-heading"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">Locator Profile</a></div>
                            <div id="collapseOne" class="accordion-body collapse in">
                                <div class="accordion-inner">
                                    <ul>
                                        <li<?= ($active==1)?' class="active"':''; ?>><a href="<?= base_url(); ?>register/locator">Basic Information</a></li>
                                        <li<?= ($active==2)?' class="active"':''; ?>><a href="<?= base_url(); ?>register/locator/personal">Personal Information</a></li>
                                        <li<?= ($active==3)?' class="active"':''; ?>><a href="<?= base_url(); ?>register/locator/residency">Residential History</a></li>
                                        <li<?= ($active==4)?' class="active"':''; ?>><a href="<?= base_url(); ?>register/locator/employment">Work History</a></li>
                                        <li<?= ($active==5)?' class="active"':''; ?>><a href="<?= base_url(); ?>register/locator/education">Education</a></li>
<?php /*
                                        <li<?= ($active==6)?' class="active"':''; ?>><a href="<?= base_url(); ?>register/locator/military">Military</a></li>
 */ ?>
                                    </ul>
                                </div> <!-- accordion-inner -->
                            </div> <!-- collapse -->
                        </div>  <!-- accordion-group -->

                        <p class="blue-text">Registration Progress</p>
                        <div class="progress progress-warning">
                        <div class="bar" style="width: <?= $progress; ?>"></div>
                        </div>


                    </div> <!-- accordion -->
                </div> <!-- well sidebar -->
            </div> <!-- span3 -->

            <!-- end Side Navigation-->


