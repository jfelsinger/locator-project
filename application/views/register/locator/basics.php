    <div class="container main-content">
        <div class="row-fluid">

            <div class="span12 patch-well">
                <div class="row-fluid">

                    <div class="progress-bar center">
                        <img src="/assets/images/ProgressBar.png" alt="progress bar icon">
                    </div>    

                    <h1>Sign Up as a Locator</h1>

                    <div class="span6">
                        <form method="post">
                            <fieldset>
                                <legend>Login Information</legend>
                                <p>Already have an account? <a href="<?= base_url(); ?>login">Login.</a></p>

                                <label for="username">Create a Username</label>
                                <?= form_error('username'); ?>
                                <input type="text" placeholder="UserName123" value="<?= set_value('username'); ?>" id="username" name="username">
                                    <div class="tipContent">
                                        "This username cannot be changed later."
                                    </div>

                                <label for="usersEmail">Your Email Address:</label>
                                <?= form_error('email'); ?>
                                <input type="email" value="<?= set_value('email'); ?>" id="usersEmail" name="email">

                                <label for="birthDate" class="birthDate">Birth Date</label>
                                <?= form_error('birthdate'); ?>
                                <input type="text" name="birthdate" value="<?= set_value('birthdate'); ?>" id="datepicker" placeholder="01/01/1970" />

                                <label for="usersPassword">Create your password</label>
                                <?= form_error('password'); ?>
                                <input type="password" value="<?= set_value('password'); ?>" id="password" name="password">
                                    <div class="tipContent">"Minimum 8 characters, including 1 number; "
                                                            "Strong passwords include special characters (e.g., #, ?, !)."
                                    </div>    

                                <label for="password2">Re-enter your password</label>
                                <?= form_error('password2'); ?>
                                <input type="password" value="<?= set_value('password2'); ?>" id="password2" name="password2">
                            </fieldset>

                            <input type="submit" name="submit" class="btn btn-medium btn-warning" value="Save &amp; Continue">
                            

                        </form>    
                    </div> <!-- span6 -->
                </div> <!-- row-fluid -->
            </div> <!-- span9 -->
        </div> <!-- row-fluid -->
    </div> <!-- container -->

    <!-- end of form-->

