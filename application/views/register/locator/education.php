    <div class="container main-content">
        <div class="row-fluid">

            <div class="span12 patch-well">
                <div class="row-fluid">

                    <div class="progress-bar center">
                        <img src="/assets/images/ProgressBar.png" alt="progress bar icon">
                    </div>    

                    <h1>Education</h1>

                    <div class="span6">
                        <form id="locEducation" method="post">
                            <input type="hidden"  name="AssociationType" id="AssociationType" value="1">

                            <fieldset> 
                                <legend>School Information</legend>
                                <label for="placeType">Type of School:</label>
                                <select name="placeType" id="placeType">
                                    <option value="1" <?= set_select('placeType', 1); ?>>University or College</option>
                                    <option value="3" <?= set_select('placeType', 3); ?>>High School</option>
                                    <option value="4" <?= set_select('placeType', 4); ?>>Middle High</option>
                                    <option value="5" <?= set_select('placeType', 5); ?>>Elementary/Primary</option>
                                    <option value="0" <?= set_select('placeType', 0); ?>>Other</option>
                                </select> 

                                <label for="placeName">Name of School:</label>
                                <input type="text" id="placeName" name="placeName" value="<?= set_value('placeName'); ?>">

                                <label for="placeCity">City:</label>
                                <input id="placeCity" name="placeCity" type="text" value="<?= set_value('placeName'); ?>">

                                <label for="placeRegion">State/Region:</label>
                                <input id="placeRegion" name="placeRegion" type="text" value="<?= set_value('placeName'); ?>">

                                <label for="placeCountry">Country:</label>
                                <select name="placeCountry" id="placeCountry">
                                <option value="1" <?= set_select('placeCountry', 1); ?>>United States</option>
                                    <option value="2" <?= set_select('placeCountry', 2); ?>>Canada</option>
                                    <option value="3" <?= set_select('placeCountry', 3); ?>>Mexico</option>
                                </select>
                            </fieldset>
                        </div>
                        <div class="span5">
                            <fieldset>
                                <legend>Date Of Attendance</legend>
                                <label for="from">From:</label>
                                <input type="text" id="from" name="dateFrom" class="datepicker" placeholder="01/01/1970" value="<?= set_value('placeName'); ?>">
                                <label for="to">To:</label>
                                <input type="text" id="to" name="dateTo" class="datepicker" placeholder="01/01/1970" value="<?= set_value('placeName'); ?>">

                            </fieldset>           

                            <?php if (isset($places) && count($places)) { ?>
                            <input type="submit" name="submit" class="btn btn-medium btn-warning" value="Add Another Place">
                            <?php } else { ?>
                            <input type="submit" name="submit" class="btn btn-medium btn-warning" value="Save &amp; Continue">
                            <?php } ?>

                            <!-- Reset button, not generally needed, might be better to give space for the skip button
                            <input type="reset" class="btn btn-medium btn-info" value="Reset">
                            -->

                            <?php if (isset($places) && count($places)) { ?>
                            <a href="<?= $skipUrl; ?>" class="btn btn-medium btn-info">Continue</a>
                            <?php } else { ?>
                            <a href="<?= $skipUrl; ?>" class="btn btn-medium btn-info">Skip</a>
                            <?php } ?>
                        </form>
                    </div> <!--span5-->        
                </div> <!-- row-fluid -->
            </div> <!-- span9 -->
        </div> <!-- row-fluid -->
    </div> <!-- container -->

    <!-- end of form-->

