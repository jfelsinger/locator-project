            <div>
                <h4>Data:</h4>
                <dl class="data">
                    <dt>Users</dt>
                        <dd><?php echo $userCount; ?></dd>
                    <dt>Notes</dt>
                        <dd><?php echo $noteCount; ?></dd>
                </dl>
                <form action="" method="POST">
                    <label for="filter">Filter</label>
                        <input type="text" id="filter" name="filter">
                    <label for="sort">Sort</label>
                        <select name="sort" id="sort">
                            <option value="1">Name</option>
                        </select>
                    <input type="submit" name="sort" value="Sort">
                </form>
                <form action="" method="POST">
                    <table cellspacing="0" class="users">
                        <thead>
                            <tr>
                                <th class="username">Username</th>
                                <th class="email">Email</th>
                                <th class="actions">Actions</th>
                                <th class="checkbox">
                                    <input type="checkbox">
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($users as $user) {
                                $id = $user->getId();
                                $username = $user->getUsername();
                                $email = $user->getEmail();
                            ?>
                            <tr>
                                <td class="username"><?php echo $username; ?></td>
                                <td class="email"><?php echo $email; ?></td>
                                <td class="actions">
                                    <a href="<?php echo base_url() . 'admin/users/view/' . $id; ?>" class="action-view">View</a>
                                    <a href="<?php echo $id; ?>" class="action-not">make note</a>
                                </td>
                                <td class="checkbox">
                                    <input type="checkbox" value="<?php echo $id; ?>" name="users[]">
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="username">Username</th>
                                <th class="email">Email</th>
                                <th class="actions">Actions</th>
                                <th class="checkbox">
                                    <input type="checkbox">
                                </th>
                            </tr>
                        </tfoot>
                    </table>

                    <label for="actions">Actions:</label>
                    <select name="actions" id="actions">
                        <option value="1" selected>Mark for Review</option>
                    </select>
                    <input type="submit" name="submit" value="Perform Action">
                </form>
            </div>
