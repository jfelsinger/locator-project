        </section><!--content end-->


        <footer>
            <!-- tbd -->
        </footer>
    </section>


    <section class="sidebar">
        <h2 class="current"><?php echo lang('page_title'); ?></h2>
        <nav>
            <ul>
                <li>
                    <a href="<?php echo base_url(); ?>admin/cases/index">Cases</a>
                    <div>
                        <ul>
                            <li><a href="<?php echo base_url(); ?>admin/cases/create">Make a case</a></li>
                            <li><a href="<?php echo base_url(); ?>admin/cases/index/new">New cases</a></li>
                            <li><a href="<?php echo base_url(); ?>admin/cases/stats">Case statistics</a></li>
                            <li><a href="<?php echo base_url(); ?>admin/cases/index/archived">Archived cases</a></li>
                        </ul>
                        <dl class="data">
                            <dt>New Cases</dt>
                                <dd>12</dd>
                            <dt>Open Cases</dt>
                                <dd>202</dd>
                            <dt>Cases Archived</dt>
                                <dd>120</dd>
                            <dt>Notes</dt>
                                <dd>30</dd>
                        </dl>
                    </div>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>admin/people/index">People</a>
                    <div>
                        <ul>
                            <li><a href="<?php echo base_url(); ?>admin/people/create">Make a person</a></li>
                            <li><a href="<?php echo base_url(); ?>admin/people/index/new">New People</a></li>
                            <li><a href="<?php echo base_url(); ?>admin/people/index/archived">Archived People</a></li>
                        </ul>
                        <dl class="data">
                            <dt></dt>
                                <dd></dd>
                        </dl>
                    </div>
                </li>
                <li>
                    <a href="<?php echo base_url(); ?>admin/places/index">Places</a>
                    <div>
                        <ul>
                            <li><a href="<?php echo base_url(); ?>admin/places/create">Make a place</a></li>
                            <li><a href="<?php echo base_url(); ?>admin/places/index/new">New Places</a></li>
                            <li><a href="<?php echo base_url(); ?>admin/places/index/archived">Archived Places</a></li>
                        </ul>
                        <dl class="data">
                            <dt></dt>
                                <dd></dd>
                        </dl>
                    </div>
                </li>
                <li>
                    <a href="#">Alerts</a>
                    <div>
                        <ul>
                            <li><a href="#">Make an Alert</a></li>
                            <li><a href="#">New Replies</a></li>
                            <li><a href="#">Alert statistics</a></li>
                        </ul>
                        <dl class="data">
                            <dt>New Replies</dt>
                                <dd>12</dd>
                            <dt>Replies Total</dt>
                                <dd>202</dd>
                            <dt>Notes</dt>
                                <dd>30</dd>
                        </dl>
                    </div>
                </li>
                <li>
                    <a href="#">Users</a>
                    <div>
                        <ul>
                            <li><a href="<?php echo base_url(); ?>admin/users/create">Make a user</a></li>
                            <li><a href="<?php echo base_url(); ?>admin/users/index/new">New users</a></li>
                            <li><a href="<?php echo base_url(); ?>admin/users/stats">User statistics</a></li>
                            <li><a href="<?php echo base_url(); ?>admin/users/index/banned">Banned users</a></li>
                        </ul>
                        <dl class="data">
                            <dt>New Users</dt>
                                <dd>12</dd>
                            <dt>Users Total</dt>
                                <dd>202</dd>
                            <dt>Banned Users</dt>
                                <dd>120</dd>
                            <dt>Administrators</dt>
                                <dd>8</dd>
                            <dt>Notes</dt>
                                <dd>30</dd>
                        </dl>
                    </div>
                </li>
                <li>
                    <a href="#">Content</a>
                    <div>
                        <ul>
                            <li><a href="#">Email Format</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Pages</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#">Settings</a>
                    <div>
                        <ul>
                            <li><a href="#">My Settings</a></li>
                            <li><a href="#">Global Settings</a></li>
                            <li><a href="#">General Statistics</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </nav>
    </section>
</body>
</html>
