            <ul class="cookieCrumbTopping">
                <li><a href="cases">Cases</a></li>
                <li><a href="cases/123">Case 123</a></li>
                <li><a href="cases/123/leads">Leads</a></li>
            </ul>
            <div>
                <h4>Data:</h4>
                <dl class="data">
                    <dt>Cases</dt>
                        <dd><?php echo $caseCount; ?></dd>
                    <dt>Notes</dt>
                        <dd><?php echo $noteCount; ?></dd>
                </dl>
                <?php if ($this->session->flashdata('success')) { ?>
                <p class="message success"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Completed Successfully'; ?></p>
                <?php } else if ($this->session->flashdata('failure')) { ?>
                <p class="message failure"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Failed'; ?></p>
                <?php } ?>
                <form action="" method="POST">
                    <label for="filter">Filter</label>
                    <input type="text" id="filter" name="filter" value="<?php echo set_value('filter'); ?>">
                    <label for="sort">Sort</label>
                        <select name="sort" id="sort">
                            <option value="id" <?php echo set_select('sort', 'id'); ?>>Id</option>
                            <option value="number" <?php echo set_select('sort', 'number'); ?>>Number</option>
                            <option value="name" <?php echo set_select('sort', 'name'); ?>>Name</option>
                            <option value="startDate" <?php echo set_select('sort', 'startDate'); ?>>Date Started</option>
                            <option value="endDate" <?php echo set_select('sort', 'endDate'); ?>>Date Ended</option>
                            <option value="lastModified" <?php echo set_select('sort', 'lastModified'); ?>>Newly Modified</option>
                            <option value="description" <?php echo set_select('sort', 'description'); ?>>Description</option>
                        </select>
                    <input type="submit" name="submitFilter" value="Sort" />
                </form>
                <?= form_open($submit_location); ?>
                    <table class="leads">
                        <tr class="heading">
                            <th class="status">Status</th>
                            <th class="name">Target</th>
                            <th class="number">Number</th>
                            <th class="actions">Actions</th>
                            <th class="checkbox">
                                <input type="checkbox">
                            </th>
                        </tr>
                        <?php
                        foreach ($cases as $case) {
                            $person = $case->getTargetPerson();

                            $id = $case->getId();
                            $number = $case->getNumber();
                            $status = $case->getStatus();
                            $name = $person->getFName() . ' ' . $person->getLName();
                        ?>
                        <tr>
                            <td class="status"><?php echo $status; ?></td>
                            <td class="name"><?php echo $name; ?></td>
                            <td class="number"><?php echo $number; ?></td>
                            <td class="actions">
                                <a href="<?php echo base_url() . 'admin/cases/view/' . $id; ?>" class="action-view">view</a>
                                <a href="<?php echo $id; ?>" class="action-mark">mark for review</a>
                                <a href="<?php echo $id; ?>" class="action-note">make note</a>
                                <a href="<?php echo $id; ?>" class="action-rate">rate</a>
                            </td>
                            <td class="checkbox">
                                <input type="checkbox" value="<?php echo $id; ?>" name="cases[]">
                            </td>
                        </tr>
                        <?php } ?>
                        <!-- I repeat the heading again at the bottom so that one does not have to be
                             at the top to see what a particular row-collumn is -->
                        <tr class="heading">
                            <th class="status">Status</th>
                            <th class="name">Target</th>
                            <th class="number">Number</th>
                            <th class="actions">Actions</th>
                            <th class="checkbox">
                                <input type="checkbox">
                            </th>
                        </tr>
                    </table>
                    <?= $this->pagination->create_links(); ?>

                    <label for="action">Actions:</label>
                    <select name="action" id="action">
                        <option value="mark" selected>Mark for Review</option>
                        <option value="archive" selected>Archive</option>
                        <option value="delete" selected>Delete</option>
                    </select>
                    <input type="submit" name="submit" value="Perform Action">
                </form>
            </div>
