            <script type="text/javascript">
                function cancel() {
                    location.href = "/admin/people/index";
                    return false;
                }
            </script>
            <article>
                <header>
                    <h2>Confirm Delete</h2>
                </header>
                <section class="col100">
                    <p>Are you sure that you would like to delete these <?= $count; ?> people? This could have unexpected side effects if any of the people being deleted have a user associated with them, or if they are related to an active case.</p>
                    <form class="confirm" method="post">
                        <?php foreach ($people as $person) { ?>
                        <input type="hidden" value="<?= $person->getId(); ?>" name="people[]" />
                        <?php } ?>
                        <input type="hidden" name="confirm" value="1" />
                        <input type="hidden" name="actions" value="delete" />

                        <input type="submit" value="Confirm" name="submit" />
                        <button type="button" onclick="cancel()">Cancel</button>
                    </form>
                </section>
            </article>
