            <div>
                <hgroup>
                    <h2>New Place</h2>
                </hgroup>
                <?php if ($this->session->flashdata('success')) { ?>
                <p class="message success"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Completed Successfully'; ?></p>
                <?php } else if ($this->session->flashdata('failure')) { ?>
                <p class="message failure"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Failed'; ?></p>
                <?php } ?>
                <?php echo form_open($submit_location); ?>
                    <input type="submit" name="submit" value="Submi">

                    <label for="placeName">Place Name</label>
                    <input type="text" name="placeName" value="<?= set_value('placeName'); ?>" placeholder="Miami University">

                    <label for="address">Address</label>
                    <input type="text" name="address1" value="<?= set_value('address1'); ?>" placeholder="Address 1">
                    <input type="text" name="address2" value="<?= set_value('address2'); ?>" placeholder="Address 2">

                    <label for="city">City</label>
                    <input type="text" name="city" value="<?= set_value('city'); ?>" placeholder="Miami">

                    <label for="region">Region</label>
                    <input type="text" name="region" value="<?= set_value('region'); ?>" placeholder="Florida">

                    <label for="zipcode">Zipcode</label>
                    <input type="text" name="zipcode" value="<?= set_value('zipcode'); ?>" placeholder="12345">

                    <label for="country">Country</label>
                    <select name="country">
                        <?php foreach($countries as $ct) { 
                            $default = ($ct['id'] == 1) 
                        ?>
                        <option value="<?= $ct['id']; ?>" <?= set_select('country', $ct['id'], $default); ?>><?= $ct['name']; ?></option>
                        <?php } ?>
                    </select>

                    <label for="placeType">Place Type</label>
                    <select name="placeType">
                        <?php foreach($placeTypes as $pt) { ?>
                        <option value="<?= $pt['id']; ?>" <?= set_select('placeType', $pt['id']); ?>><?= $pt['type']; ?></option>
                        <?php } ?>
                    </select>
                    <input type="text" name="otherText" value="<?= set_value('type'); ?>" placeholder="Gas Station">

                    <label for="description">Description</label>
                    <textarea name="description"><?= set_value('description'); ?></textarea>

                    <input type="submit" name="submit" value="Submit">
                </form>
            </div>
