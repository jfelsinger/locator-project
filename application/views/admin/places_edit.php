            <div>
                <hgroup>
                    <h2>Edit Place</h2>
                    <h4><?= $place->getName(); ?></h4>
                </hgroup>
                <?php if ($this->session->flashdata('success')) { ?>
                <p class="message success"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Completed Successfully'; ?></p>
                <?php } else if ($this->session->flashdata('failure')) { ?>
                <p class="message failure"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Failed'; ?></p>
                <?php } ?>
                <?= validation_errors(); ?>
                <?php echo form_open($submit_location); ?>
                    <input type="submit" name="submit" value="Submit">

                    <label for="placeName">Place Name</label>
                    <input type="text" name="placeName" value="<?= $place->getName(); ?>" placeholder="Miami University">

                    <label for="address">Address</label>
                    <input type="text" name="address1" value="<?= $place->getAddress1(); ?>" placeholder="Address 1">
                    <input type="text" name="address2" value="<?= $place->getAddress2(); ?>" placeholder="Address 2">

                    <label for="city">City</label>
                    <input type="text" name="city" value="<?= $place->getCity(); ?>" placeholder="Miami">

                    <label for="region">Region</label>
                    <input type="text" name="region" value="<?= $place->getRegion(); ?>" placeholder="Florida">

                    <label for="zipcode">Zipcode</label>
                    <input type="text" name="zipcode" value="<?= $place->getZipcode(); ?>" placeholder="12345">

                    <label for="country">Country</label>
                    <select name="country">
                        <?php foreach($countries as $ct) {
                            $default = ($ct['id'] == $place->getCountryId());
                        ?>
                        <option value="<?= $ct['id']; ?>" <?= set_select('country', $ct['id'], $default); ?>><?= $ct['name']; ?></option>
                        <?php } ?>
                    </select>

                    <label for="placeType">Place Type</label>
                    <select name="placeType">
                        <?php foreach ($placeTypes as $pt) { 
                            $default = ($pt['id'] == $place->getPlaceTypeId());
                        ?>
                        <option value="<?= $pt['id']; ?>" <?= set_select('placeType', $pt['id'], $default); ?>><?= $pt['type']; ?></option>
                        <?php } ?>
                    </select>
                    <input type="text" name="otherText" value="<?= ($place->getPlaceTypeId()==0) ? $place->getOtherText() : ''; ?>" placeholder="Gas Station">

                    <label for="description">Description</label>
                    <textarea name="description"><?= $place->getDescription(); ?></textarea>

                    <input type="submit" name="submit" value="Submit">
                </form>
            </div>
