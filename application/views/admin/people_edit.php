            <div>
                <hgroup>
                    <h2>Edit Person</h2>
                    <h4><?php echo $person->getFName() . ' ' . $person->getLName(); ?></h4>
                </hgroup>
                <?php if ($this->session->flashdata('success')) { ?>
                <p class="message success"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Completed Successfully'; ?></p>
                <?php } else if ($this->session->flashdata('failure')) { ?>
                <p class="message failure"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Failed'; ?></p>
                <?php } ?>
                <?php echo form_open($submit_location); ?>
                    <input type="submit" name="submit" value="Submit">

                    <label for="personActive">Include this person in search data</label>
                    <div class="radioSwitch greenSwitch">
                        <input type="radio" name="personActive" value="0" id="activeNo" <?php if ($person->getActive() == 0) echo ' checked';?>>
                        <input type="radio" name="personActive" value="1" id="activeYes" <?php if ($person->getActive() != 0) echo ' checked';?>>
                        <span class="switch"></span>
                        <label for="activeNo">No</label>
                        <label for="activeYes" class="right">Yes</label>
                    </div>

                    <label for="personFName">First Name</label>
                    <input type="text" name="personFName" value="<?php echo $person->getFName(); ?>" id="personFName" placeholder="ex. John">

                    <label for="personmName">Middle Name</label>
                    <input type="text" name="personMName" value="<?php echo $person->getMName(); ?>" id="personMName" placeholder="ex. A. or Arnold">

                    <label for="personLName">Last Name</label>
                    <input type="text" name="personLName" value="<?php echo $person->getLName(); ?>" id="personLName" placeholder="ex. Doe">

                    <label for="personMaName">Maiden Name</label>
                    <input type="text" name="personMaName" value="<?php echo $person->getMaName(); ?>" id="personMaName" placeholder="ex. Richardson">

                    <label for="personGender">Gender</label>
                    <div class="radioSwitch genderSwitch">
                        <input id="genderMale" type="radio" name="personGender" value="male"<?php if ($person->getGender() != 'female') echo ' checked'; ?>>
                        <input id="genderFemale" type="radio" name="personGender" value="female"<?php if ($person->getGender() == 'female') echo ' checked'; ?>>
                        <span class="switch"></span>                        
                        <label for="genderMale">Male</label>
                        <label for="genderFemale" class="right">Female</label>
                    </div>

                    <label for="personBirthdate">Birth Date</label>
                    <?php echo form_error('personBirthdate'); ?>
                    <input type="text" name="personBirthdate" class="datepicker" id="personBirthdate" value="<?php echo $person->getBirthdate('%m/%d/%Y'); ?>" placeholder="ex. 12-23-2015" class="datePicker">

                    <label for="personRace">Race</label>
                    <select name="personRace" id="personRace">
                        <option value="1">Unknown</option>
                        <option value="2">Race 2</option>
                        <option value="3">Race 3</option>
                        <option value="4">Race 4</option>
                    </select>

                    <label for="personAdopted">Adopted</label>
                    <div class="radioSwitch adoptedSwitch">
                        <input id="adoptedYes" type="radio" name="personAdopted" value="0"<?php if ($person->getAdopted() == 0) echo ' checked'; ?>>
                        <input id="adoptedNo" type="radio" name="personAdopted" value="1"<?php if ($person->getAdopted() != 0) echo ' checked'; ?>>
                        <span class="switch"></span>
                        <label for="adoptedNo">No</label>
                        <label for="adoptedYes" class="right">Yes</label>
                    </div>

                    <?php
                    /*
                     *  a bunch of stuff to be added in the event that they were adopted,
                     *  a default place to be prompted for, among other things.
                     */
                    ?>

                    <input type="submit" name="submit" value="Submit">
                </form>
            </div>
