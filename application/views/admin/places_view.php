            <article>
                <header>
                    <form class="right" name="quickActions" method="POST" onSubmit="return redirect('<?php echo base_url() . 'admin/places/'; ?>')">
                        <label for="actions">Actions</label>
                        <select name="action" id="actions">
                            <option value="edit" selected>Edit</option>
                        </select>
                        <input type="submit" name="submit" value="Go">
                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                    </form>
                    <script type="text/javascript">
                        function redirect(loc) {
                            var val = quickActions.action.value,
                                id = quickActions.id.value;
                            switch (val) {
                                case 'edit':
                                    loc += 'edit/' + id;
                                    break;
                                case 'archive':
                                    loc += 'archive/' + id;
                                    break;
                                default:
                            }
                            document.location = loc;
                            return false;
                        }
                    </script>
                    <hgroup>
                        <h2><?php echo $place->getName(); ?></h2>
                        <h4>View Place</h4>
                    </hgroup>
                    <dl class="data">
                        <dt>Created on</dt>
                            <dd><?php echo $place->getCreatedDate('%d/%m/%Y'); ?></dd>
                    </dl>
                </header>
                <?php if ($this->session->flashdata('success')) { ?>
                <p class="message success"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Completed Successfully'; ?></p>
                <?php } else if ($this->session->flashdata('failure')) { ?>
                <p class="message failure"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Failed'; ?></p>
                <?php } ?>
                <section class="col100">
                    <h4>Information</h4>
                    <dl>
                        <dt>Type</dt>
                            <dd><?php echo $place->getType(); ?></dd>
                        <dt>Name</dt>
                            <dd><?php echo $place->getName(); ?></dd>
                        <dt>Description</dt>
                            <dd><?php echo $place->getDescription(); ?></dd>
                        <dt>Address</dt>
                            <dd><?php echo $place->getAddress1(); ?></dd>
                            <dd><?php echo $place->getAddress2(); ?></dd>
                        <dt>city</dt>
                            <dd><?php echo $place->getCity(); ?></dd>
                        <dt>region</dt>
                            <dd><?php echo $place->getRegion(); ?></dd>
                        <dt>zipcode</dt>
                            <dd><?php echo $place->getZipcode(); ?></dd>
                        <dt>country</dt>
                            <dd><?php echo $place->getCountryId(); ?></dd>
                    </dl>
                </section><!--information end-->
                <section class="bottomList">
                    <h4>People</h4>
                    <?php if (count($related)) { ?>
                    <ul>
                        <?php foreach($related as $r) { ?>
                        <li>
                            <input type="checkbox" class="select">
                            <h5><a href="<?= base_url() . 'admin/people/view/'. $r['person']->getId(); ?>"><?= $r['person']->getFName() .' '. $r['person']->getLName(); ?></a></h5>
                            <a href="#" class="more"></a>
                            <div class="more">
                                <dl>
                                    <dt>From</dt>
                                        <dd><?= $r['connection']->getStartDate('%m/%d/%Y'); ?></dd>
                                    <dt>To</dt>
                                        <dd><?= $r['connection']->getEndDate('%m/%d/%Y'); ?></dd>
                                </dl>
                                <a href="<?= base_url() . 'admin/places/editConnection/'. $r['connection']->getId(); ?>" class="more">Edit Connection</a>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                    <?php } else { ?>
                    <p>None</p>
                    <?php } ?>
                </section>
                <section class="notes">
                    <h5>Notes</h5>
                    <small class="count">24</small>
                    <article class="note">
                        <footer>
                            <p>Author: George Admin</p>
                            <p>
                                <time datetime="234">posted time</time>
                            </p>
                        </footer>
                        <p>This is the note</p>
                    </article>
                    <article class="note">
                        <footer>
                            <p>Author: George Admin</p>
                            <p>
                                <time datetime="234">posted time</time>
                            </p>
                        </footer>
                        <p>This is the note</p>
                    </article>
                </section><!--notes end-->
            </article>
