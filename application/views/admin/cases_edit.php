            <div>
                <hgroup>
                    <h2>Edit Case</h2>
                    <h4><?php echo $case->getName(); ?></h4>
                </hgroup>
                <?php echo form_open($submit_location); ?>
                    <input type="submit" name="submit" value="Submit">

                    <label for="caseNumber">Case Number</label>
                    <?php echo form_error('caseNumber'); ?>
                    <input type="text" id="caseNumber" name="caseNumber" value="<?php echo $case->getNumber(); ?>" placeholder="ex. 324">

                    <label for="caseName"><span class="optional">opt.</span> Case Name</label>
                    <input type="text" id="caseName" name="caseName" value="<?php echo $case->getName(); ?>" placeholder="ex. The Fredrickson Case">

                    <label for="actions">Set Priority</label>
                    <select name="casePriority" id="priority">
                        <option value="1"<?php if ($case->getStatus() == 1) echo ' checked'; ?>>Low Priority</option>
                        <option value="2"<?php if ($case->getStatus() == 2) echo ' checked'; ?>>Medium Priority</option>
                        <option value="3"<?php if ($case->getStatus() == 3) echo ' checked'; ?>>High Priority</option>
                    </select>

                    <label for="caseDescription"><span class="optional">opt.</span> Case Description</label>
                    <textarea name="caseDescription" id="caseDescription"><?php echo $case->getDescription(); ?></textarea>

                    <?php /*
                    <hr>
                
                    <h4>The Target</h4>

                    <label for="personFName">First Name</label>
                    <input type="text" name="personFName" value="<?php echo $person->getFName(); ?>" id="personFName" placeholder="ex. John">

                    <label for="personmName">Middle Name</label>
                    <input type="text" name="personMName" value="<?php echo $person->getMName(); ?>" id="personMName" placeholder="ex. A. or Arnold">

                    <label for="personLName">Last Name</label>
                    <input type="text" name="personLName" value="<?php echo $person->getLName(); ?>" id="personLName" placeholder="ex. Doe">

                    <label for="personMaName">Maiden Name</label>
                    <input type="text" name="personMaName" value="<?php echo $person->getMaName(); ?>" id="personMaName" placeholder="ex. Richardson">

                    <label for="personGender">Gender</label>
                    <div class="radioSwitch genderSwitch">
                        <input id="genderMale" type="radio" name="personGender" value="0"<?php if ($person->getGender() != 'female') echo ' checked'; ?>>
                        <input id="genderFemale" type="radio" name="personGender" value="1"<?php if ($person->getGender() == 'female') echo ' checked'; ?>>
                        <span class="switch"></span>                        
                        <label for="genderMale">Male</label>
                        <label for="genderFemale" class="right">Female</label>
                    </div>

                    <label for="personBirthdate">Birth Date</label>
                    <?php echo form_error('personBirthdate'); ?>
                    <input type="text" name="personBirthdate" class="datepicker" id="personBirthdate" value="<?php echo $person->getBirthdate('%m/%d/%Y'); ?>" placeholder="ex. 12-23-2015" class="datePicker">

                    <label for="personRace">Race</label>
                    <select name="personRace" id="personRace">
                        <option value="1">Unknown</option>
                        <option value="2">Race 2</option>
                        <option value="3">Race 3</option>
                        <option value="4">Race 4</option>
                    </select>

                    <label for="personAdopted">Adopted</label>
                    <div class="radioSwitch adoptedSwitch">
                        <input id="adoptedYes" type="radio" name="personAdopted" value="0"<?php if ($person->getAdopted() == 0) echo ' checked'; ?>>
                        <input id="adoptedNo" type="radio" name="personAdopted" value="1" <?php if ($person->getAdopted() == 1) echo ' checked'; ?>>
                        <span class="switch"></span>
                        <label for="adoptedNo">No</label>
                        <label for="adoptedYes" class="right">Yes</label>
                    </div>

                    */ ?>

                    <input type="submit" name="submit" value="Submit">
                </form>
            </div>
