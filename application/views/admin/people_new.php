            <div>
                <hgroup>
                <h3>New Person</h3>
                <h2><?php echo set_value('personName'); ?></h2>
                </hgroup>
                <?php if ($this->session->flashdata('success')) { ?>
                <p class="message success"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Completed Successfully'; ?></p>
                <?php } else if ($this->session->flashdata('failure')) { ?>
                <p class="message failure"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Failed'; ?></p>
                <?php } ?>
                <?php echo form_open($submit_location); ?>
                    <input type="submit" name="submit" value="Submit">

                    <?php echo form_error('caseNumber'); ?>

                    <label for="personActive">Include this person in search data</label>
                    <div class="radioSwitch greenSwitch">
                        <input type="radio" name="personActive" value="0" id="activeNo"  <?php set_radio('personActive', 0, true);?>>
                        <input type="radio" name="personActive" value="1" id="activeYes" <?php set_radio('personActive', 1);?>>
                        <span class="switch"></span>
                        <label for="activeYes" >Yes</label>
                        <label for="activeNo" class="right">No</label>
                    </div>

                    <label for="personFName">First Name</label>
                    <input type="text" name="personFName" value="<?php echo set_value('personFName'); ?>" id="personFName" placeholder="ex. John">

                    <label for="personmName">Middle Name</label>
                    <input type="text" name="personMName" value="<?php echo set_value('personMName'); ?>" id="personMName" placeholder="ex. A. or Arnold">

                    <label for="personLName">Last Name</label>
                    <input type="text" name="personLName" value="<?php echo set_value('personLName'); ?>" id="personLName" placeholder="ex. Doe">

                    <label for="personMaName">Maiden Name</label>
                    <input type="text" name="personMaName" value="<?php echo set_value('personMaName'); ?>" id="personMaName" placeholder="ex. Richardson">

                    <label for="personGender">Gender</label>
                    <div class="radioSwitch genderSwitch">
                        <input id="genderMale" type="radio" name="personGender" value="0"   <?php echo set_radio('personGender', 0); ?>>
                        <input id="genderFemale" type="radio" name="personGender" value="1" <?php echo set_radio('personGender', 1, true); ?>>
                        <span class="switch"></span>                        
                        <label for="genderMale">Male</label>
                        <label for="genderFemale" class="right">Female</label>
                    </div>

                    <label for="personBirthdate">Birth Date</label>
                    <?php echo form_error('personBirthdate'); ?>
                    <input type="text" name="personBirthdate" class="datepicker" id="personBirthdate" value="<?php echo set_value('personBirthdate'); ?>" placeholder="ex. 12-23-2015" class="datePicker">

                    <label for="personRace">Race</label>
                    <select name="personRace" id="personRace">
                        <option value="1">Unknown</option>
                        <option value="2">Race 2</option>
                        <option value="3">Race 3</option>
                        <option value="4">Race 4</option>
                    </select>

                    <label for="personAdopted">Adopted</label>
                    <div class="radioSwitch adoptedSwitch">
                        <input id="adoptedYes" type="radio" name="personAdopted" value="0" <?php echo set_radio('personAdopted', '0', true); ?>>
                        <input id="adoptedNo" type="radio" name="personAdopted" value="1"  <?php echo set_radio('personAdopted', '1'); ?>>
                        <span class="switch"></span>
                        <label for="adoptedNo">No</label>
                        <label for="adoptedYes" class="right">Yes</label>
                    </div>
                    
                    <fieldset class="orphanageInfo">
                        <legend>Orphanage Info</legend>
                        <div class="ui-widget autoPlace">
                            <label for="personOrphanage">Orphanage Name:</label>
                            <input type="text" name="orphName" id="personOrphanage" class="autocomplete places" value="<?= set_value('orphName'); ?>" placeholder="Orphanage Name">
                            <input type="hidden" name="personPlaceId" class="dataId" id="personPlaceId" value="<?php echo set_value('personPlaceId'); ?>">
                            <input type="hidden" name="type" value="college/university">
                        </div>

                        <label for="orphAddress1">Address</label>
                        <input type="text" id="orphAddress1" class="address1" name="orphanageAddress1" value="<?php echo set_value('orphAddress1'); ?>" placeholder="Address 1">
                        <input type="text" id="orphAddress2" class="address2" name="orphAddress2" value="<?php echo set_value('orphAddress2'); ?>" placeholder="Address 2">

                        <label for="orphCity">City</label>
                        <input type="text" id="orphCity" class="city" value="<?php echo set_value('orphCity'); ?>" name="orphCity" placeholder="Cape Coral">

                        <label for="orphZipcode">Zipcode</label>
                        <input type="text" id="orphZipcode" class="zipcode" placeholder="55555" name="orphZipcode" value="<?php echo set_value('orphZipcode'); ?>">

                        <div class="ui-widget autoCountry">
                            <label for="orphCountry">Country</label>
                            <input type="text" name="orphCountry" class="autocomplete country" value="<?php echo set_value('orphCountry'); ?>" placeholder="United States">
                            <input type="hidden" name="orphCountryId" class="dataId" value="<?php echo set_value('orphCountryId'); ?>">
                        </div>
                        </select>
                    </fieldset>

                    <?php
                    /*
                     *  a bunch of stuff to be added in the event that they were adopted,
                     *  a default place to be prompted for, among other things.
                     */
                    ?>

                    <input type="submit" name="submit" value="Submit">
                </form>
            </div>
