            <article>
                <header>
                    <form class="right" name="quickActions" method="POST" onSubmit="return redirect('<?php echo base_url() . 'admin/cases/'; ?>')">
                        <label for="actions">Actions</label>
                        <select name="action" id="actions">
                            <option value="edit" selected>Edit</option>
                            <option value="archive">Archive</option>
                            <option value="1">Mark as viewed</option>
                            <option value="2">Mark as important</option>
                            <option value="3">Mark normal priority</option>
                        </select>
                        <input type="submit" name="submit" value="Go">
                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                    </form>
                    <script type="text/javascript">
                        function redirect(loc) {
                            var val = quickActions.action.value,
                                id = quickActions.id.value;
                            switch (val) {
                                case 'edit':
                                    loc += 'edit/' + id;
                                    break;
                                case 'archive':
                                    loc += 'archive/' + id;
                                    break;
                                default:
                            }
                            document.location = loc;
                            return false;
                        }
                    </script>
                    <hgroup>
                        <h2><?php echo $case->getName(); ?></h2>
                        <h4>View Case</h4>
                    </hgroup>
                    <dl class="data">
                        <dt>Status</dt>
                            <dd><?php echo $case->getStatus(); ?></dd>
                        <dt>Started by</dt>
                            <dd><?php echo $startedBy; ?></dd>
                        <dt>on</dt>
                            <dd><?php echo $case->getStartDate('%d/%m/%Y'); ?></dd>
                    </dl>
                </header>

                <p><?php echo $case->getDescription(); ?></p>

                <section class="colh">
                    <h4>Seeker</h4>
                    <dl>
                        <dt>Name</dt>
                        <dd>Seeker Name</dd>
                    </dl>
                    <a href="#" class="more">Go to seeker</a>

                </section>

                <section class="target colh last">
                    <h4>Target</h4>
                    <dl>
                        <dt>Name</dt>
                            <dd><?php echo $target->getFName() . ' ' . $target->getLName(); ?></dd>
                    </dl>
                    <a href="<?= base_url() . 'admin/people/view/' . $target->getId(); ?>" class="more">Review Target</a>
                </section>

                <section class="notes">
                    <h5>Notes</h5>
                    <small class="count">24</small>
                    <article class="note">
                        <footer>
                            <p>Author: George Admin</p>
                            <p>
                                <time datetime="234">posted time</time>
                            </p>
                        </footer>
                        <p>This is the note</p>
                    </article>
                    <article class="note">
                        <footer>
                            <p>Author: George Admin</p>
                            <p>
                                <time datetime="234">posted time</time>
                            </p>
                        </footer>
                        <p>This is the note</p>
                    </article>
                </section>
            </article>
