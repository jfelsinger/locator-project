            <div>
                <h4>Data:</h4>
                <dl class="data">
                    <dt>People</dt>
                        <dd><?php echo $personCount; ?></dd>
                    <dt>Notes</dt>
                        <dd><?php echo $noteCount; ?></dd>
                </dl>
                <?php if ($this->session->flashdata('success')) { ?>
                <p class="message success"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Completed Successfully'; ?></p>
                <?php } else if ($this->session->flashdata('failure')) { ?>
                <p class="message failure"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Failed'; ?></p>
                <?php } ?>
                <form method="POST">
                    <label for="filter">Filter</label>
                        <input type="text" id="filter" name="filter" value="<?php echo set_value('filter'); ?>">
                    <label for="sort">Sort</label>
                        <select name="sort" id="sort">
                            <option value="fName" <?php echo set_select('sort', 'fName'); ?>>First Name</option>
                            <option value="lName" <?php echo set_select('sort', 'lName'); ?>>Last Name</option>
                            <option value="mName" <?php echo set_select('sort', 'mName'); ?>>Middle Name</option>
                            <option value="maName" <?php echo set_select('sort', 'maName'); ?>>Maiden Name</option>
                            <option value="suffix" <?php echo set_select('sort', 'suffix'); ?>>Suffix</option>
                            <option value="gender" <?php echo set_select('sort', 'gender'); ?>>Gender</option>
                            <option value="birthdate" <?php echo set_select('sort', 'birthdate'); ?>>Birthdate</option>
                            <option value="createdDate" <?php echo set_select('sort', 'createdDate'); ?>>Date Created</option>
                            <option value="id" <?php echo set_select('sort', 'id'); ?>>Id</option>
                        </select>
                    <input type="submit" name="submitFilter" value="Sort" />
                </form>
                <?= form_open($submit_location); ?>
                    <table class="people">
                        <tr class="heading">
                            <th class="status">Type</th>
                            <th class="name">Name</th>
                            <th class="number">Age</th>
                            <th class="actions">Actions</th>
                            <th class="checkbox">
                                <input type="checkbox">
                            </th>
                        </tr>
                        <?php
                        foreach ($people as $person) {

                            $id = $person->getId();
                            $status = $person->getType();
                            $name = $person->getFName() . ' ' . $person->getLName();
                            $age = strftime('%Y') - $person->getBirthdate('%Y');
                        ?>
                        <tr>
                            <td class="status"><?php echo $status; ?></td>
                            <td class="name"><?php echo $name; ?></td>
                            <td class="number"><?php echo $age; ?></td>
                            <td class="actions">
                                <a href="<?php echo base_url() . 'admin/people/view/' . $id; ?>" class="action-view">view</a>
                                <a href="<?php echo $id; ?>" class="action-mark">mark for review</a>
                                <a href="<?php echo $id; ?>" class="action-note">make note</a>
                                <a href="<?php echo $id; ?>" class="action-rate">rate</a>
                            </td>
                            <td class="checkbox">
                                <input type="checkbox" value="<?php echo $id; ?>" name="people[]">
                            </td>
                        </tr>
                        <?php } ?>
                        <!-- I repeat the heading again at the bottom so that one does not have to be
                             at the top to see what a particular row-collumn is -->
                        <tr class="heading">
                            <th class="status">Status</th>
                            <th class="name">Name</th>
                            <th class="number">Age</th>
                            <th class="actions">Actions</th>
                            <th class="checkbox">
                                <input type="checkbox">
                            </th>
                        </tr>
                    </table>
                    <?= $this->pagination->create_links(); ?>

                    <label for="actions">Actions:</label>
                    <select name="actions" id="actions">
                        <option value="review" selected>Mark for Review</option>
                        <option value="delete" selected>Delete</option>
                    </select>
                    <input type="submit" name="submit" value="Perform Action">
                </form>
            </div>
