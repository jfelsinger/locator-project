            <div>
                <h4>Data:</h4>
                <dl class="data">
                    <dt>People</dt>
                        <dd><?php echo $placeCount; ?></dd>
                    <dt>Notes</dt>
                        <dd><?php echo $noteCount; ?></dd>
                </dl>
                <?php if ($this->session->flashdata('success')) { ?>
                <p class="message success"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Completed Successfully'; ?></p>
                <?php } else if ($this->session->flashdata('failure')) { ?>
                <p class="message failure"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Failed'; ?></p>
                <?php } ?>
                <form action="" method="POST">
                    <label for="filter">Filter</label>
                    <input type="text" id="filter" name="filter" value="<?php echo set_value('filter'); ?>">
                    <label for="sort">Sort</label>
                        <select name="sort" id="sort">
                            <option value="name" <?php echo set_select('sort', 'name'); ?>>Name</option>
                            <option value="address1" <?php echo set_select('sort', 'address1'); ?>>Address 1</option>
                            <option value="address2" <?php echo set_select('sort', 'address2'); ?>>Address 2</option>
                            <option value="city" <?php echo set_select('sort', 'city'); ?>>City</option>
                            <option value="region" <?php echo set_select('sort', 'region'); ?>>Region</option>
                            <option value="zipcode" <?php echo set_select('sort', 'zipcode'); ?>>Zipcode</option>
                            <option value="createdDate" <?php echo set_select('sort', 'createdDate'); ?>>Date Created</option>
                            <option value="description" <?php echo set_select('sort', 'description'); ?>>Description</option>
                        </select>
                    <input type="submit" name="submitFilter" value="Sort" />
                </form>
                <?= form_open($submit_location); ?>
                    <table class="places">
                        <tr class="heading">
                            <th class="name">Name</th>
                            <th class="placeType">Type</th>
                            <th class="country">Country</th>
                            <th class="actions">Actions</th>
                            <th class="checkbox">
                                <input type="checkbox">
                            </th>
                        </tr>
                        <?php
                        foreach ($places as $place) {

                            $id = $place->getId();
                            $name = $place->getName();
                            $type = $place->getType();
                            $country = $place->getCountryId();
                        ?>
                        <tr>
                            <td class="name"><?php echo $name; ?></td>
                            <td class="placeType"><?php echo $type; ?></td>
                            <td class="country"><?php echo $country; ?></td>
                            <td class="actions">
                                <a href="<?php echo base_url() . 'admin/places/view/' . $id; ?>" class="action-view">view</a>
                                <a href="<?php echo $id; ?>" class="action-note">make note</a>
                            </td>
                            <td class="checkbox">
                                <input type="checkbox" value="<?php echo $id; ?>" name="places[]">
                            </td>
                        </tr>
                        <?php } ?>
                        <!-- I repeat the heading again at the bottom so that one does not have to be
                             at the top to see what a particular row-collumn is -->
                        <tr class="heading">
                            <th class="name">Name</th>
                            <th class="placeType">Type</th>
                            <th class="country">Country</th>
                            <th class="actions">Actions</th>
                            <th class="checkbox">
                                <input type="checkbox">
                            </th>
                        </tr>
                    </table>
                    <?= $this->pagination->create_links(); ?>

                    <label for="actions">Actions:</label>
                    <select name="actions" id="actions">
                        <option value="1" selected>Mark for Review</option>
                    </selected>
                    <input type="submit" name="submit" value="Perform Action">
                </form>
            </div>
