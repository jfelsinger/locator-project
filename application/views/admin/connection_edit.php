            <div>
                <hgroup>
                    <h2>Edit Connection</h2>
                    <h4><?= $person->getFName() . ' ' . $person->getLName(); ?> to <?= $place->getName(); ?></h4>
                </hgroup>
                <?= form_open($submit_location); ?>
                    <input type="submit" name="submit" value="Submit">

                    <label for="associationId">Connection Type</label>
                    <?= form_error('associationId'); ?>
                    <select name="associationId" id="associationId">
                        <?php foreach ($associationTypes as $assType) { 
                        $default = ($assType['id'] == $connection->getAssociationId());
                        ?>
                        <option value="<?= $assType['id']; ?>" <?= set_select('associationId', $assType['id'], $default); ?>><?= $assType['type']; ?></option>
                        <?php } ?>
                    </select>

                    <label for="otherText">Other</label>
                    <input type="text" name="otherText" id="otherText" value="<?= ($connection->getAssociationId() == 0)? $connection->getType() : ''; ?>">

                    <label for="startDate">From</label>
                    <input type="text" class="datepicker" name="startDate" id="startDate" value="<?= $connection->getStartDate('%Y-%m-%d'); ?>">

                    <label for="endDate">To</label>
                    <input type="text" class="datepicker" name="endDate" id="endDate" value="<?= $connection->getEndDate('%Y-%m-%d'); ?>">

                    <input type="submit" name="submit" value="Submit">
                </form>
            </div>
