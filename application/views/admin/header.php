<!DOCTYPE html>
<html>
<head>
    <meta charset="utf8">
    <title><?php echo lang('page_title'); ?></title>

    <link href="<?php echo base_url(); ?>assets/admin/css/jquery-ui.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/admin/css/main.css" rel="stylesheet" type="text/css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/main.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/autocomplete.js" type="text/javascript"></script>
</head>

<!--HTML5 Document-->

<body>
    <section class="main">
        <header>
            <h1 class="logo"><a href="#">The Locator Backend</a></h1>

            <section class="userInfo">
                <!-- Not 100% on what would go here yet -->
                <p class="welcome">Welcome, Your name here</p>
                <p>Is this not you? <a href="#" class="logout">logout</a></p>
                <p><a href="#">settings</a></p>
            </section>
        </header>


        <section class="content">
