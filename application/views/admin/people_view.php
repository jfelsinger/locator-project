            <article>
                <header>
                    <form class="right" name="quickActions" method="POST" onSubmit="return redirect('<?php echo base_url() . 'admin/people/'; ?>')">
                        <label for="actions">Actions</label>
                        <select name="action" id="actions">
                            <option value="edit" selected>Edit</option>
                            <option value="archive">Archive</option>
                            <option value="1">Mark as viewed</option>
                        </select>
                        <input type="submit" name="submit" value="Go">
                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                    </form>
                    <script type="text/javascript">
                        function redirect(loc) {
                            var val = quickActions.action.value,
                                id = quickActions.id.value;
                            switch (val) {
                                case 'edit':
                                    loc += 'edit/' + id;
                                    break;
                                case 'archive':
                                    loc += 'archive/' + id;
                                    break;
                                default:
                            }
                            document.location = loc;
                            return false;
                        }
                    </script>
                    <hgroup>
                        <h2><?php echo $person->getFName() . ' ' . $person->getLName(); ?></h2>
                        <h4>View Person</h4>
                    </hgroup>
                    <small class="status"><?php echo $person->getType(); ?></small>
                    <dl class="data">
                        <dt>Created on</dt>
                            <dd><?php echo $person->getCreatedDate('%d/%m/%Y'); ?></dd>
                        <dt>Active</dt>
                            <dd><?php echo ($person->getActive())? 'Yes' : 'No'; ?></dd>
                        <dt>Adopted</dt>
                            <dd><?php echo ($person->getAdopted())? 'Yes' : 'No'; ?></dd>
                    </dl>
                </header>
                <?php if ($this->session->flashdata('success')) { ?>
                <p class="message success"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Completed Successfully'; ?></p>
                <?php } else if ($this->session->flashdata('failure')) { ?>
                <p class="message failure"><?= ($this->session->flashdata('message')) ? $this->session->flashdata('message') : 'Action Failed'; ?></p>
                <?php } ?>
                <section class="col100">
                    <h4>Information</h4>
                    <dl>
                        <dt>First Name</dt>
                            <dd><?php echo $person->getFName();?></dd>
                        <dt>Middle Name</dt>
                            <dd><?php echo $person->getMName();?></dd>
                        <dt>First Name</dt>
                            <dd><?php echo $person->getLName();?></dd>

                        <dt>Maiden Name</dt>
                            <dd><?php echo $person->getMaName(); ?></dd>

                        <dt>Gender</dt>
                            <dd><?php echo $person->getGender(); ?></dd>
                        <dt>Birthdate</dt>
                            <dd><?php echo $person->getBirthdate('%d/%m/%Y'); ?></dd>
                        <dt>Race</dt>
                            <dd><?php echo $person->getRace(); ?></dd>
                    </dl>
                </section>
                <section class="bottomList">
                    <?php // Places will be populated with ptp_bridge and place information ?>
                    <h4>Places</h4>
                    <?php if (count($places)) { ?>
                    <ul>
                        <?php foreach($places as $place) { ?>
                        <li>
                            <input type="checkbox" class="select">
                            <h5><a href="<?= base_url() . 'admin/places/view/'. $place['location']->getId(); ?>"><?= $place['location']->getName(); ?></a></h5>
                            <div class="more">
                                <dl>
                                    <dt>From</dt>
                                        <dd><?= $place['connection']->getStartDate('%m/%d/%Y'); ?></dd>
                                    <dt>To</dt>
                                        <dd><?= $place['connection']->getEndDate('%m/%d/%Y'); ?></dd>
                                </dl>
                                <a href="<?= base_url() . 'admin/people/editConnection/'. $place['connection']->getId(); ?>" class="more">Edit Connection</a>
                            </div>

                        </li>
                        <?php } ?>
                    </ul>
                    <?php } else { ?>
                    <p>None</p>
                    <?php } ?>
                </section>
                <section class="notes">
                    <h5>Notes</h5>
                    <small class="count">24</small>
                    <article class="note">
                        <footer>
                            <p>Author: George Admin</p>
                            <p>
                                <time datetime="234">posted time</time>
                            </p>
                        </footer>
                        <p>This is the note</p>
                    </article>
                    <article class="note">
                        <footer>
                            <p>Author: George Admin</p>
                            <p>
                                <time datetime="234">posted time</time>
                            </p>
                        </footer>
                        <p>This is the note</p>
                    </article>
                </section>
            </article>
