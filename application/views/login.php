    <div class="container main-content">
        <div class="row-fluid">

            <div class="span12 patch-well">
                <div class="row-fluid">



                    <div class="span8 offset4">

                        <h1>Login</h1>

                        <form method="post">
                            <div>

                                <a href="#"><img id="face" src="assets/images/logos/facebook-logo-55X55.png" alt="facebook icon"></a>
                                <a href="#"><img src="assets/images/logos/linkedin-logo-55X55.png" alt="linkedin icon"></a>
                                <!--<a href="#"><img src="img/google.png" alt="google plus icon"></a> -->

                            </div>
                            <br/>

                            <?php if ($this->session->flashdata('message')) { ?>
                            <div class="error">
                            <?= $this->session->flashdata('message'); ?>
                            </div>
                            <?php } ?>

                            <fieldset>
                                <label for="identity">Enter username or email </label>
                                <?= form_error('identity'); ?>
                                <input type="text" name="identity" id="identity" value="<?= set_value('identity'); ?>">

                                <label for="password">Password</label>
                                <?= form_error('password'); ?>
                                <input type="password" name="password" id="password" value="<?= set_value('password'); ?>">

                                <!--
                                <a href="#">Forgot?</a>
                                -->

                                <label class="checkbox">
                                    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?> Remember Me</label>
                            </fieldset>

                            <input type="submit" class="btn btn-medium btn-warning" value="Login">


                        </form> 
                    </div> <!--span8-->        
                </div> <!-- row-fluid -->
            </div> <!-- span9 -->
        </div> <!-- row-fluid -->
    </div> <!-- container -->


