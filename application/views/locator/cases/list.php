    <div class="container main-content">
        <div class="row-fluid">

            <?= $sidebar; ?>
            <div class="span9 patch-well">
                <div class="row-fluid">

                    <h2>Cases</h2>

                    <div class="span6">
                        <form method="post">
                            <fieldset>
                                <legend>Cases List</legend>
                                <table class="table table-striped table-condensed">
                                    <thead>
                                        <th>Status</th>
                                        <th>Name</th>
                                        <th>Number</th>
                                        <th>Actions</th>
                                        <th class="checkbox">
                                            <input type="checkbox">
                                        </th>
                                    </thead>
                                    <tr>
                                        <td class="status">Status</td>
                                        <td>Name</td>
                                        <td>Number</td>
                                        <td>Actions</td>
                                        <td class="checkbox">
                                            <input type="checkbox">
                                        </td>
                                    </tr>
                                    <tfoot>
                                        <th>Status</th>
                                        <th>Name</th>
                                        <th>Number</th>
                                        <th>Actions</th>
                                        <th class="checkbox">
                                            <input type="checkbox">
                                        </th>
                                    </tfoot>
                                </table>
                            </fieldset>
                        </form>    
                    </div>
                </div> <!-- row-fluid -->
            </div> <!-- span9 -->
        </div> <!-- row-fluid -->
    </div> <!-- container -->

    <!-- end of form-->




