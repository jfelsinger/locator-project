<?php
    if (!isset($progress))
        $progress = "10";

    if (!isset($active))
        $active = 0;
?>
            <!--Side Navigation-->  

            <div class="span4">
                <div class="well sidebar-nav">
                    <div class="accordion" id="accordion2">
                        <div class="accordion-group">
                            <div class="accordion-heading"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">Locator Profile</a></div>
                            <div id="collapseOne" class="accordion-body collapse in">
                                <div class="accordion-inner">
                                    <ul>
                                        <li<?= ($active==1)?' class="active"':''; ?>><a href="<?= base_url(); ?>user">Account Settings</a></li>
                                        <li<?= ($active==2)?' class="active"':''; ?>><a href="<?= base_url(); ?>user/personal">Personal Info</a></li>
                                        <li<?= ($active==3)?' class="active"':''; ?>><a href="<?= base_url(); ?>user/residency">Residential History</a></li>
                                        <li<?= ($active==4)?' class="active"':''; ?>><a href="<?= base_url(); ?>user/employment">Work History</a></li>
                                        <li<?= ($active==5)?' class="active"':''; ?>><a href="<?= base_url(); ?>user/education">Education</a></li>
                                        <?php /*
                                        <li<?= ($active==6)?' class="active"':''; ?>><a href="<?= base_url(); ?>user/military">Military</a></li>
                                        */ ?>
                                    </ul>
                                </div> <!-- accordion-inner -->
                            </div> <!-- collapse -->
                        </div>  <!-- accordion-group -->

                        <?php if ($progress < 100) { ?>
                        <p class="blue-text">Account Completion</p>
                        <div class="progress progress-warning">
                            <div class="bar" style="width: <?= $progress; ?>%"></div>
                        </div>
                        <small>
                            <p>There is important information missing from your profile. The more information in your profile, the more alerts you will receive.</p>
                        </small>
                        <?php } ?>
                    </div> <!-- accordion -->
                </div> <!-- well sidebar -->
            </div> <!-- span3 -->

            <!-- end Side Navigation-->



