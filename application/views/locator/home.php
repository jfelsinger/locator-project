<!-- START PROFILE PAGE -->

     <div class="container main-content">
        
        <div class="row-fluid profileHeaderBox">
            <div class="span12">
                <div class="span5 offset1">
                    <h1>User Name</h1>
                </div>
                <div class="span5">
                    <img src="assets/images/warning-icon.png" class="pull-right profileIcon">
                    <img src="assets/images/badge-icon-white.png" class="pull-right profileIcon">
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12 patch-well">
                <div class="row-fluid">

                    <div class="span5 offset1">
                        <div class="profileBox">
                            <h2>Personal Information</h2>
                            <a href="#"><p><img src="assets/images/pencil-orange-20x20.png"> Edit</p></a>
                        </div>
                        
                        <div class="profileBox">
                            <h2>Residential Information</h2>
                            <a href="#"><p><img src="assets/images/pencil-orange-20x20.png"> Edit</p></a>
                        </div>

                        <div class="profileBox">
                            <h2>Education Information</h2>
                            <a href="#"><p><img src="assets/images/pencil-orange-20x20.png"> Edit</p></a>
                        </div>
                    </div>
            
                    
                    <div class="span5">
                        <div class="profileBox">
                            <h2>Settings</h2>
                            <a href="#"><p><img src="assets/images/pencil-orange-20x20.png"> Edit</p></a>
                        </div>
                        
                        <div class="profileBox">
                            <h2>Work Information</h2>
                            <a href="#"><p><img src="assets/images/pencil-orange-20x20.png"> Edit</p></a>
                        </div>

                        <div class="profileBox">
                            <h2>Military Information</h2>
                            <a href="#"><p><img src="assets/images/pencil-orange-20x20.png"> Edit</p></a>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>