    <div class="container main-content">
        <div class="row-fluid">

            <?= $sidebar; ?>
            <div class="span9 patch-well">
                <h2>Residency</h2>

                <form method="post">
                    <fieldset>
                        <legend>
                            Residency List <a href="<?= base_url() . 'user/residency/add'; ?>" class="btn btn-info pull-right">Add New Place</a>
                        </legend>
                        <table class="table table-condensed">
                            <thead>
                                <th>Name</th>
                                <th>Country</th>
                                <th>Actions</th>
                                <!--
                                <th class="checkbox">
                                    <input type="checkbox">
                                </th>
                                -->
                            </thead>
                            <?php if (isset($places) &&count($places)) {
                                foreach ($places as $place) { 
                                $id = $place->getId();
                            ?>
                            <tr class="hover">
                                <td><?= $place->getName() ? $place->getName() : $place->getAddress1(); ?></td>
                                <td><?= $place->getCountry(); ?></td>
                                <td>
                                    <a href="<?= base_url() . 'user/education/view' . $id; ?>">View</a>
                                    | <a href="<?= base_url() . 'user/education/edit' . $id; ?>">Edit</a>
                                    | <a href="<?= base_url() . 'user/education/remove' . $id; ?>">Remove</a>
                                </td>
                                <!--
                                <td class="checkbox">
                                    <input value="<?= $id; ?>" type="checkbox">
                                </td>
                                -->
                            </tr>
                            <?php } 
                            } else { ?>
                            <tr>
                                <td class="center" colspan=5>You have no places of residence in your profile. <a href="<?= base_url() . 'user/education/add'; ?>">Add a new place</a>?</td>
                            </tr>
                            <?php } ?>
                            <tfoot>
                                <th>Name</th>
                                <th>Country</th>
                                <th>Actions</th>
                                <!--
                                <th class="checkbox">
                                    <input type="checkbox">
                                </th>
                                -->
                            </tfoot>
                        </table>
                    </fieldset>
                </form>    
            </div> <!-- span9 -->
        </div> <!-- row-fluid -->
    </div> <!-- container -->

    <!-- end of form-->



