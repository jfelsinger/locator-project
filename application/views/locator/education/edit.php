    <div class="container main-content">
        <div class="row-fluid">

            <?= $sidebar; ?>
            <div class="span9 patch-well">
                <div class="row-fluid">

                    <h2>Education</h2>

                    <div class="span6">
                        <form method="post">
                            <fieldset>
                                <legend>School Information</legend>
                                <!--Form for editing current education-->
                                <label for="placeType">School Type</label>
                                <select name="placeType" id="placeType">
                                </select>

                                <label for="placeName">School Name:</label>
                                <input type=text" id="placeName" name="placeName">

                                <label for="placeCity">City:</label>
                                <input type="text" name="placeCity" id="placeCity">

                                <label for="placeRegion">State/Region:</label>
                                <input type="text" name="placeRegion" id="placeRegion">
                            </fieldset>
                        </form>    
                    </div>
                </div> <!-- row-fluid -->
            </div> <!-- span9 -->
        </div> <!-- row-fluid -->
    </div> <!-- container -->

    <!-- end of form-->


