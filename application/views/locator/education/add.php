    <div class="container main-content">
        <div class="row-fluid">

            <?= $sidebar; ?>
            <div class="span8 patch-well">
                <div class="row-fluid">

                    <h2>Education</h2>

                    <?= validation_errors(); ?>

                    <form id="locEducation" method="post">
                        <div class="span6">
                            <input type="hidden"  name="AssociationType" id="AssociationType" value="1">

                            <fieldset> 
                                <legend>School Information</legend>
                                <label for="placeType">Type of School:</label>
                                <select name="placeType" id="placeType">
                                    <option value="1" <?= set_select('placeType', 1); ?>>University or College</option>
                                    <option value="3" <?= set_select('placeType', 3); ?>>High School</option>
                                    <option value="4" <?= set_select('placeType', 4); ?>>Middle High</option>
                                    <option value="5" <?= set_select('placeType', 5); ?>>Elementary/Primary</option>
                                    <option value="0" <?= set_select('placeType', 0); ?>>Other</option>
                                </select> 

                                <div class="ui-widget autoPlace">
                                    <label for="placeName">Name of School:</label>
                                    <input type="text" class="autocomplete places" id="placeName" name="placeName" value="<?= set_value('placeName'); ?>">
                                    <input type="hidden" name="placeId" class="dataId" id="placeId" value="<?= set_value('placeId'); ?>">
                                    <input type="hidden" name="type" value="school">
                                </div>

                                <label for="placeCity">City:</label>
                                <input id="placeCity" name="placeCity" class="city" type="text" value="<?= set_value('placeCity'); ?>">

                                <label for="placeRegion">State/Region:</label>
                                <input id="placeRegion" class="region" name="placeRegion" type="text" value="<?= set_value('placeRegion'); ?>">

                                <select class="country" name="placeCountry" id="placeCountry">
                                    <?php foreach ($countries as $country) { ?>
                                    <option value="<?= $country['value']; ?>" <?= set_select('placeCountry', $country['value']); ?>><?= $country['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </fieldset>
                        </div>
                        <div class="span5">
                            <fieldset>
                                <legend>Date Of Attendance</legend>
                                <label for="from">From:</label>
                                <input type="text" id="from" name="dateFrom" class="datepicker" placeholder="mm/dd/yyyy" value="<?= set_value('dateFrom'); ?>">
                                <label for="to">To:</label>
                                <input type="text" id="to" name="dateTo" class="datepicker" placeholder="mm/dd/yyyy" value="<?= set_value('dateTo'); ?>">

                            </fieldset>           

                            <input type="submit" name="submit" class="btn btn-medium btn-warning" value="Add Place">
                            <a href="<?= base_url(); ?>user/education/list" class="btn btn-medium btn-info">Cancel</a>
                        </div> <!--span5-->        
                    </form>
                </div> <!-- row-fluid -->
            </div> <!-- span8 -->
        </div> <!-- row-fluid -->
    </div> <!-- container -->

    <!-- end of form-->


