    <div class="container main-content">
        <div class="row-fluid">

            <?= $sidebar; ?>
            <div class="span8 patch-well">
                <h2>Education</h2>

                <form method="post">
                    <fieldset>
                        <legend>
                            Education List <a href="<?= base_url() . 'user/education/add'; ?>" class="btn btn-info pull-right">Add New Place</a>
                        </legend>
                        <table class="table table-condensed">
                            <thead>
                                <th>Name</th>
                                <th>Dates</th>
                                <th>Actions</th>
                                <!--
                                <th class="checkbox">
                                    <input type="checkbox">
                                </th>
                                -->
                            </thead>
                            <?php if (isset($bridges) && count($bridges)) {
                                foreach ($bridges as $bridge) { 
                                $place = $bridge->getPlace();
                                $id = $bridge->getId();
                            ?>
                            <tr class="hover">
                                <td><?= $place->getName() ? $place->getName() : $place->getAddress1(); ?></td>
                                <td><?= $bridge->getStartDate('%m/%d/%Y') . " - " . $bridge->getEndDate('%m/%d/%Y'); ?></td>
                                <td>
                                    <a href="<?= base_url() . 'user/education/view/' . $id; ?>">View</a>
                                    | <a href="<?= base_url() . 'user/education/edit/' . $id; ?>">Edit</a>
                                    | <a href="<?= base_url() . 'user/education/remove/' . $id; ?>">Remove</a>
                                </td>
                                <!--
                                <td class="checkbox">
                                    <input value="<?= $id; ?>" type="checkbox">
                                </td>
                                -->
                            </tr>
                            <?php } 
                            } else { ?>
                            <tr>
                                <td class="center" colspan=5>You have no places of education in your profile. <a href="<?= base_url() . 'user/education/add'; ?>">Add a new place</a>?</td>
                            </tr>
                            <?php } ?>
                            <tfoot>
                                <th>Name</th>
                                <th>Dates</th>
                                <th>Actions</th>
                                <!--
                                <th class="checkbox">
                                    <input type="checkbox">
                                </th>
                                -->
                            </tfoot>
                        </table>
                    </fieldset>
                </form>    
            </div> <!-- span9 -->
        </div> <!-- row-fluid -->
    </div> <!-- container -->

    <!-- end of form-->


