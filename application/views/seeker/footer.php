
     <!--footer-->

     <footer class="footer">
         <div class="container">
             <div class="shadow-effect">
                 <div class="row-fluid">
                     <div class="span12 center">
                         <ul class="footer-links inline">
                             <li class="active"><a href="home">Home</a></li>
                             <li><a href="about/troy">About</a></li>
                             <li><a href="faq">FAQ</a></li>
                             <li><a href="contact">Contact</a></li>
                             <li><a href="about/terms-of-use">Terms of Use</a></li>
                             <li><a href="login">Login</a></li>
                         </ul>
                     </div> <!--span12-->
                 </div> <!--row-fluid-->

                 <div class="row-fluid">
                     <div class="span12">
                         <div class="center">  
                             <img src="assets/images/Troy-Alert-Small.png" alt="Troy Alert Logo">
                             <p>© 2013 Troy's Alert. All rights reserved.</p>
                             <p>Programmed and Designed by <a href="about/team"><strong>Web Development and Programming at High Tech North</strong></a></p>
                         </div> <!-- center -->
                     </div>  <!-- span12 -->
                 </div>  <!--row-fluid -->
             </div>  <!--shadow-effect-->
         </div> <!-- container -->
     </footer>

     <!-- end of footer-->

     <!--<script src="http://code.jquery.com/jquery-latest.js"></script> --> 
     <script src="assets/js/bootstrap.js"></script> 
     <script src="assets/js/imsky-holder-a1201ab/holder.js"></script>
 </body>
 </html>

