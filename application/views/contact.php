     <div class="container">
         <div class="shadow-effect main-content">
             <div class="row-fluid pattern-background">
                 <div id="contact-form" class="span9 offset1">
                     <h1>Contact Us</h1>
                     <h4>Fill out the form below to get in touch with us.</h4>

                     <form method="post" action="">
                         <label for="name">Name: *</span></label>
                     <input type="text" id="name" name="name" value="" placeholder="John Doe" required autofocus />
                     <label for="email">Email Address: *</label>
                     <input type="email" id="email" name="email" value="" placeholder="johndoe@example.com" required />
                     <label for="subject">Subject: *</label>
                     <select id="subject" name="subject" required="required">
                         <option value="general">General Inquiry</option>
                         <option value="technical">Technical Issues</option>
                         <option value="media">Media Inquiry</option>
                     </select>

                     <label for="message">Message: *</label>
                     <textarea id="message" name="message" required></textarea>
                     <input class="btn btn-medium btn-warning"type="submit" value="Submit" id="submit" />
                 </form>


                 <h4>Media Inquiries</h4>
                 <p>If this is a media inquiry, send your FULL contact information including FULL details about the media outlet you are with.</p>
                 <h4>Additional Help</h4>
                 <p>If you are seeking help from Troy Dunn and his team other than what is offered here at TroyAlert, try any of these additional options:</p>
                 <ul>
                     <li>Get help from a professional or tell your story to the producers on Troy’s tv appearances: <a href="http://www.troythelocator.com">TroyTheLocator.com</a></li>
                     <li>Have Troy teach you how to do your own search via video instruction and get access to Troy’s lead investigators by joining <a href="http://www.LocatorClub.com">LocatorClub.com.</a></li>
                     <li>Still not reunited? Join the world’s fastest growing (and FREE!) reunion registry at <a href="http://www.TroysList.org">TroysList.org.</a></li>
                 </ul>
                 <h4>General Inquiries</h4>
                 <p>Please note these are not reviewed frequently and not all inquiries sent here get a reply.</p>

             </div>
         </div>
     </div>
 </div>


