<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "index";
$route['404_override'] = '';

/***
 * Administration Routes
 ***/

// Person -> People
$route['admin/person/(:any)/(:num)']    = "admin/people/$1/$2";
$route['admin/person/(:any)']           = "admin/people/$1";
$route['admin/person']                  = "admin/people";

// Case -> Cases
$route['admin/case/(:any)/(:num)']      = "admin/cases/$1/$2";
$route['admin/case/(:any)']             = "admin/cases/$1";
$route['admin/case']                    = "admin/cases";

// Place -> Places
$route['admin/place/(:any)/(:num)']     = "admin/places/$1/$2";
$route['admin/place/(:any)']            = "admin/places/$1";
$route['admin/place']                   = "admin/places";

// Users && User -> Admin_auth
$route['admin/users/(:any)/(:num)']     = "admin/admin_auth/$1/$2";
$route['admin/users/(:any)']            = "admin/admin_auth/$1";
$route['admin/users']                   = "admin/admin_auth";
$route['admin/user/(:any)/(:num)']      = "admin/admin_auth/$1/$2";
$route['admin/user/(:any)']             = "admin/admin_auth/$1";
$route['admin/user']                    = "admin/admin_auth";

$route['admin/login']                   = "admin/admin_auth/login";


/***
 * Client Routes
 ***/

$route['about/terms-of-use']            = "about/terms";
$route['about/high-tech-north']         = "about/team";
$route['about/htn']                     = "about/team";
$route['frequently-asked-questions']    = "faqs";

// $route['login']                         = "auth/login";
$route['user/forgot-password']          = "auth/forgot_password";
$route['user/reset-password/(:any)']    = "auth/reset_password/$1";
$route['user/reset-password']           = "auth/reset_password";

$route['register/locator-or-seeker']    = "register/locator_or_seeker";
$route['register/(:any)/(:any)/(:any)'] = "register/$2/$3/$1";
$route['register/(:any)/thank-you']     = "register/thank_you/index/$1";
$route['register/(:any)/(:any)']        = "register/$2/index/$1";
$route['register/(:any)']               = "register/basics/index/$1";
$route['register']                      = "register/locator_or_seeker";


/* End of file routes.php */
/* Location: ./application/config/routes.php */
