<?php
/**
 * Note: Fuck, I am going back to documenting all the methods at the top
 *
 * __call(string , bool encode_html_output=true)    // Magic getter and setter
 * 
 * public::
 * save($keynames=false)         // null : save the object to the db
 * delete()                      // null : deletes the  obj's data from the db
 * null()                        // null : wipes all of the obj's data, but keeps it in the db
 * getRelated(
 *      $className, 
 *      $colName=false, 
 *      $offset=0, $limit=null, 
 *      $sortOrder='id', 
 *      $table=false, 
 *      $field='id')            // array : This function makes me sad, :<
 *
 * static::
 * deleteData($id)              // null : deletes the obj for the data with the given id
 * dataExists($id)              // bool : checks to see if data exists for the given id
 * getMany(
 *      $offset=0,
 *      $limit=null,
 *      $sortOrder='id'
 *      $where=false)           // array : returns an array of the specified objects
 * getCount(
 *      $where=false,
 *      $table=false)           // int : returns the ammount of objects in the db
 */
    class MY_Model extends CI_Model {
        protected static $db;
        const TABLE_NAME = 'undefined';

        function __construct() {
            parent::__construct();
            self::$db = &get_instance()->db;
        }

        protected
            $private = array(),
            $changed = array();

        static
            $columnNames = array(); // propertyName -> columnName

        public function __call($name, $arguments) {
            if (strpos($name, 'get') === 0) {
                // getProperty()
                $name = lcfirst(substr($name, 3));
                if (array_key_exists($name, static::$columnNames))
                    return (!isset($arguments[0]) || $arguments[0] !== false) ? htmlspecialchars($this->$name) : $this->$name;
                else
                    throw new LogicException('Property "' .$name. '" is not accessible.');
            }
            else if (strpos($name, 'set') === 0) {
                // setProperty(val)
                $name = lcfirst(substr($name, 3));
                if (!in_array($name, $this->private) && array_key_exists($name, static::$columnNames)) {
                    $this->$name = $arguments[0];
                    if (!in_array($name, $this->changed))
                        $this->changed[] = $name;
                } else
                    throw new LogicException('Property "' .$name. '" is not accessible.');
            }
        }

        public function getColumnNames() {
            return static::$columnNames;
        }

        public function save($keyNames = false) {
            if ($keyNames === false) // so that it can be overwritten, only  define if not set
               $keyNames = array();

            if (!count($this->changed) && !count($keyNames))
                return;

            foreach ($this->changed as $key => $value) {
                // If it is already  set from a supplied array
                // unset it and move on
                if (array_key_exists($value, $keyNames)) {
                    unset($this->changed[$key]);
                    continue;
                }
                // escape the value, using the key from the changed array
                if (!is_numeric($this->$value))
                    $escValue = $this->db->escape($this->$value);
                else
                    $escValue = $this->$value;

                // Add the escaped values to the keyNames array
                if (array_key_exists($value, static::$columnNames))
                    $keyNames[static::$columnNames[$value]] = $escValue;
                else
                    $keyNames[$value] = $escValue;

                // unset so that there are no leftovers when save is next called
                unset($this->changed[$key]);
            }
            if ($this->id) {
                // If the data object exists already edit it
                // If something has an id it should have a corresponding row in the db
                // by default, id should only be set in constructor
                $sqlValues = array();
                foreach ($keyNames as $key => $value) {
                    $sqlValues[] = "`{$key}`={$value}";
                }
                $sqlValues = implode(', ', $sqlValues);
                $sql = "UPDATE " . static::TABLE_NAME . " SET $sqlValues WHERE `id`='{$this->id}' LIMIT 1";
            } else {
                // else, create a new data object and save it into the db
                $sqlValues = array();
                $sqlFields = array();
                foreach ($keyNames as $key => $value) {
                    $sqlValues[] = $value;
                    $sqlFields[] = $key;
                }
                $sqlValues = implode(', ', $sqlValues);
                $sqlFields = implode(', ', $sqlFields);

                $sql = "INSERT INTO " . static::TABLE_NAME . " ({$sqlFields}) VALUES ({$sqlValues})";
            }

            $query = $this->db->query($sql);
            if (!$this->id)
                $this->id = $this->db->insert_id();
        }

        public function delete() {
            if ($this->id) {
                $sql = "DELETE FROM " . static::TABLE_NAME . " WHERE `id`={$this->id}";
                $query = $this->db->query($sql);
            } else
                throw new Exception('Data object has no id, cannot delete');
        }

        public static function deleteData($id) {
            if ($id) {
                $sql = "DELETE FROM " . static::TABLE_NAME . " WHERE `id`=$id";
                $query = self::$db->query($sql);
            } else
                throw new Exception('invalid id');
        }

        public static function dataExists($id) {
            $sql = "SELECT COUNT(*) AS c FROM " . static::TABLE_NAME . " WHERE `id`=$id";
            $query = self::$db->query($sql);
            $row = $query->row();
            return ($row->c)? true : false;
        }

        public static function getMany($offset=0, $limit=null, $sortOrder='id', $where=false, $join=false) {
            $data = array();
            $limit = (($limit == null)? '18446744073709551615' : $limit);
            $c = get_called_class();
            
            if ($sortOrder)
                $sortOrder = "`{$sortOrder}`";

            if ($where)
                $where = 'WHERE '.$where;

            $sql = "SELECT `". $c::TABLE_NAME ."`.id FROM `". $c::TABLE_NAME ."` $join $where ORDER BY {$sortOrder} LIMIT {$offset} , {$limit}";

            $query = self::$db->query($sql);
            foreach ($query->result() as $row)
                $data[] = new $c($row->id);

            return $data;
        }

        public static function getCount($where=false, $table=false) {
            $table = ($table)? $table : static::TABLE_NAME;
            if ($where !== false)
                $where = "WHERE $where";
            $sql = "SELECT COUNT(*) AS c FROM $table $where";

            $query = self::$db->query($sql);
            $row = $query->row();

            return (int) $row->c;
        }

        protected function getRelated($connectorClass, $connectorCol=false, $offset='0', $limit=null, $sortOrder='id', $table=false, $field='id') {
            // This function needs some work, holy crap
            $this->load->model($connectorClass);
            $table = ($table) ? $table : $connectorClass::TABLE_NAME ;
            $connections = array();
            $limit = (($limit == null)? '18446744073709551615' : $limit);

            $sql = "SELECT $field FROM $table WHERE `$connectorCol`={$this->id} ORDER BY {$sortOrder} LIMIT {$offset} , {$limit}";
            $query = $this->db->query($sql);
            foreach ($query->result_array() as $row)
                $connections[] = new $connectorClass($row[$field]);

            return $connections;
        }

    }
?>
