<?php
class Secure_Controller extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		//Require memebers to be logged in. If not logged in, redirect.
		if( !$this->ion_auth->logged_in() )
		{
            redirect(base_url() . 'login', 'refresh');
		}
	}
}
