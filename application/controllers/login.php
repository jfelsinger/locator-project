<?php
class Login extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->library('Form_validation');
    }

    public function index() {
        $this->load->library('Form_validation');
        $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
        $this->form_validation->set_rules('identity', 'Identity', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run()) {
            $remember = (bool) $this->input->post('remember');

            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                // if successful redirect them to their homepage
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect(base_url() , 'refresh');
            } else {
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect(base_url() . 'login', 'refresh');
            }

        } else {
            $data['message'] = ($this->session->flashdata('message'));

            $this->load->view('header');
            $this->load->view('login', $data);
            $this->load->view('footer');
        }
    }
}
?>
