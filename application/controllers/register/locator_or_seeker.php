<?php
class Locator_or_seeker extends CI_Controller {
    
    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('header');
        $this->load->view('register/locator-or-seeker');
        $this->load->view('footer');
    }
}
?>
