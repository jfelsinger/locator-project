<?php
class Residency extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('Ptp_bridge_model');
        $this->load->model('Person_model');
        $this->load->model('Place_model');
    }

    public function index($type=null) {
        if ($type == null)
            redirect(base_url() . "register/locator-or-seeker");

        if ($this->input->post('submit') != false) {
            if ($this->input->post('placeId')) {
                $place = new Place_model( $this->input->post('placeId') );
            } else {
                $place = new Place_model();

                if ($this->input->post('placeName'))
                    $place->setName( $this->input->post('placeName') );

                if ($this->input->post('placeAddress1'))
                    $place->setAddress1( $this->input->post('placeAddress1') );

                if ($this->input->post('placeAddress2'))
                    $place->setAddress2( $this->input->post('placeAddress2') );

                if ($this->input->post('placeCity'))
                    $place->setCity( $this->input->post('placeCity') );

                if ($this->input->post('placeRegion'))
                    $place->setRegion( $this->input->post('placeRegion') );

                if ($this->input->post('placeCountry'))
                    $place->setCountryId( $this->input->post('placeCountry') );

                $place->setPlaceTypeId(10);

                $place->save();
            }

            $person = new Person_model( $this->session->userdata('personId') );
            $bridge = $place->connectPerson($person);

            if ($this->input->post('startDate'))
                $bridge->setStartDate( strtotime($this->input->post('startDate')) );

            if ($this->input->post('endDate'))
                $bridge->setEndDate( $this->input->post('endDate') );

            if ($this->input->post('associationType'))
                $bridge->setAssociationId( strtotime($this->input->post('associationType')) );

            $bridge->save();

            redirect(base_url() . "register/" . $type . "/employment");
        }

        $sidebarData['progress'] = "0";
        $sidebarData['active'] = 3;
        $data['sidebar'] = $this->parser->parse('register/'.$type.'/sidebar', $sidebarData, TRUE);
        $data['countries'] = $this->locator->getCountries();
        $data['skipUrl'] = base_url() . "register/" . $type . "/employment";

        $this->load->view('header');
        $this->load->view('register/'.$type.'/residency', $data);
        $this->load->view('footer');
    }
}
?>
