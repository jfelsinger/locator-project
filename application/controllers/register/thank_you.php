<?php
class Thank_you extends CI_Controller {
    
    function __construct() {
        parent::__construct();
    }

    public function index($type=null) {
        if ($type == null)
            redirect(base_url() . "register/locator-or-seeker");

        $this->load->view('header');
        $this->load->view('register/' . $type . '/thank-you');
        $this->load->view('footer');
    }
}
?>
