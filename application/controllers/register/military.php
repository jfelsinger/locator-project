<?php
class Military extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('Ptp_bridge_model');
        $this->load->model('Person_model');
        $this->load->model('Place_model');
    }

    public function index($type=null) {
        if (true || $type == null) // No one should be here currently, will be implemented later
            redirect(base_url() . "register/locator-or-seeker");

        if ($this->input->post('submit') != false) {
            if ($this->input->post('placeId')) {
                $place = new Place_model( $this->input->post('placeId') );
            } else {
                $place = new Place_model();

                if ($this->input->post('placeName'))
                    $place->setName( $this->input->post('placeName') );

                if ($this->input->post('placeAddress1'))
                    $place->setAddress1( $this->input->post('placeAddress1') );

                if ($this->input->post('placeAddress2'))
                    $place->setAddress2( $this->input->post('placeAddress2') );

                if ($this->input->post('placeCity'))
                    $place->setCity( $this->input->post('placeCity') );

                if ($this->input->post('placeRegion'))
                    $place->setRegion( $this->input->post('placeRegion') );

                if ($this->input->post('placeCountry'))
                    $place->setCountryId( $this->input->post('placeCountry') );

                if ($this->input->post('placeType'))
                    $place->setPlaceTypeId( $this->input->post('placeType') );

                $place->save();
            }

            $person = new Person_model( $this->session->userdata('personId') );
            $bridge = $place->connectPerson($person);

            if ($this->input->post('startDate'))
                $bridge->setStartDate( strtotime($this->input->post('startDate')) );

            if ($this->input->post('endDate'))
                $bridge->setEndDate( $this->input->post('endDate') );

            if ($this->input->post('associationType'))
                $bridge->setAssociationId( strtotime($this->input->post('associationType')) );

            $bridge->save();

            redirect(base_url() . "register/" . $type . "/thank-you");
        }

        $sidebarData['progress'] = "0";
        $sidebarData['active'] = 6;
        $data['sidebar'] = $this->parser->parse('register/'.$type.'/sidebar', $sidebarData, TRUE);
        $data['skipUrl'] = base_url() . "register/" . $type . "/thank-you";

        $this->load->view('header');
        $this->load->view('register/'.$type.'/military', $data);
        $this->load->view('footer');
    }
}
?>
