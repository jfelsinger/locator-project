<?php
class Basics extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->model('Person_model');
        $this->load->model('User_model');
    }

    public function index($type=null) {
        // If the registration track has not ben specified, for some reason
        // put them back to the choice page
        if ($type == null)
            redirect(base_url() . "register/locator-or-seeker");

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if ($this->input->post('submit') != false) {
            $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
            $this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]');
            $this->form_validation->set_rules('email', 'Email', 'required|is_unique[users.email]|valid_email');
            $this->form_validation->set_rules('birthdate', 'Birthdate', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('password2', 'Password Confirmation', 'required|matches[password]');

            if ($this->form_validation->run()) {
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $email = $this->input->post('email');

                $person = new Person_model();
                $person->setBirthdate(strtotime($this->input->post('birthdate')));
                $person->setActive(true);
                $person->setType('user');
                $person->save();

                $id = $this->ion_auth->register($username, $password, $email, array(
                    'personId' => $person->getId(),
                    'active' => true
                ));


                if ($id != false) {
                    $this->session->set_userdata(array('personId' => $person->getId()));

                    redirect(base_url() . "register/" . $type . "/personal");
                }
            }
        }

        $sidebarData['progress'] = "0";
        $sidebarData['active'] = 1;
        $data['sidebar'] = $this->load->view('register/'.$type.'/sidebar', $sidebarData, TRUE);

        $this->load->view('header');
        $this->load->view('register/'.$type.'/basics', $data);
        $this->load->view('footer');
    }
}
?>
