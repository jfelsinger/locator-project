<?php
class Personal extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->model('Person_model');
        $this->load->model('User_model');
    }

    public function index($type=null) {
        if ($type == null)
            redirect(base_url() . "register/locator-or-seeker");

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if ($this->input->post('submit') != false) {
            $this->form_validation->set_error_delimiters('<p class="error">', '</p>');

            $person = $this->session->userdata('personId');
            $person = new Person_model($person);

            // Set all of the potentential fields, if they are set
            if ($this->input->post('personFName'))
                $person->setFName( $this->input->post('personFName') );

            if ($this->input->post('personMName'))
                $person->setMName( $this->input->post('personMName') );

            if ($this->input->post('personLName'))
                $person->setLName( $this->input->post('personLName') );

            if ($this->input->post('personMaName'))
                $person->setMaName( $this->input->post('personMaName') );

            if ($this->input->post('personSuffix'))
                $person->setSuffix( $this->input->post('personSuffix') );

            /* Commented out until function added
            if ($this->input->post('cell'))
                $person->addPhoneNumber( $this->input->post('cell') , 'cell');

            if ($this->input->post('home'))
                $person->addPhoneNumber( $this->input->post('home') , 'home');

            if ($this->input->post('work'))
                $person->addPhoneNumber( $this->input->post('work') , 'work');
            */

            if ($this->input->post('personGender'))
                $person->setGender( $this->input->post('personGender') );

            /* Commented out until values made accurate
            if ($this->input->post('personRace'))
                $person->setRace( $this->input->post('personRace') );
             */

            $person->save();

            redirect(base_url() . "register/" . $type . "/residency");
        }

        $sidebarData['progress'] = "0";
        $sidebarData['active'] = 2;
        $data['sidebar'] = $this->parser->parse('register/'.$type.'/sidebar', $sidebarData, TRUE);

        $data['races'] = $this->locator->getRaces();

        $this->load->view('header');
        $this->load->view('register/'.$type.'/personal', $data);
        $this->load->view('footer');
    }
}
?>
