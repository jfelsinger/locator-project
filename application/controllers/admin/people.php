<?php
class People extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Person_model');
    }

    public function actions() {
        $people = array();
        foreach ($this->input->post('people') as $p)
            $people[] = new Person_model($p);

        $this->session->set_flashdata('success', true);
        switch ($this->input->post('actions')) {
            case 'review':
                redirect('admin/people/index');
                break;
            case 'delete':
                if ($this->input->post('confirm')) {
                    $this->load->model('User_model');
                    foreach($people as $person)
                        if ($person->getType() == 'user') {
                            $user = User_model::getMany(0,1,'id','`personId`='.$person->getId());
                            if (count($user)) {
                                $user = $user[0];
                                $user->delete();
                            }
                            $person->delete();
                        } else if ($person->getType() == 'target'){
                            $this->session->set_flashdata('failure', true);
                            $this->session->set_flashdata('message', 'Some people could not be deleted');
                        } else {
                            $person->delete();
                        }
                    redirect('admin/people/index');
                } else if (count($people)) {
                    $data['people'] = $people;
                    $data['count'] = count($people);
                    $this->load->view('admin/header');
                    $this->load->view('admin/confirm/person_delete', $data);
                    $this->load->view('admin/footer');
                } else
                    redirect('admin/people/index');

                break; //// end delete case ////
            default:
                $this->session->set_flashdata('failure', true);
                redirect('admin/people/index');
        }
    }

    public function index() {
        $sortBy = 'id';
        $where = "`type` <> 'temporary'";

        if ($this->input->post('submitFilter')) {
            if (in_array($this->input->post('sort'), Person_model::$columnNames))
                $sortBy = $this->input->post('sort');

            if ($this->input->post('filter')) {
                $whereArray = array();
                $filter = '\'%'.$this->db->escape_like_str($this->input->post('filter')).'%\'';
                $whereArray[] = 'fName LIKE '.$filter;
                $whereArray[] = 'lName LIKE '.$filter;
                $whereArray[] = 'mName LIKE '.$filter;
                $whereArray[] = 'maName LIKE '.$filter;
                $whereArray[] = 'suffix LIKE '.$filter;
                $whereArray[] = 'birthdate LIKE '.$filter;
                $whereArray[] = 'birthdate LIKE \''.date('Y-m-d H:i:s', strtotime($this->input->post('filter'))).'\'';
                $where = '( '.implode(' OR ', $whereArray).' ) AND '.$where;
            }
        }

        $this->load->library('pagination');
        $config['base_url'] = base_url().'admin/people/index/';
        $data['personCount'] = $config['total_rows'] = Person_model::getCount($where);
        $config['per_page'] = 35;
        $config['uri_segment'] = 4;

        $this->pagination->initialize($config);
        $page = $this->uri->segment($config['uri_segment']);
        $page = $page ? $page : 1;

        $data['noteCount'] = 12; #Note_model::getCount();
        $data['people'] = Person_model::getMany($page-1, $config['per_page']+1, $sortBy, $where);

        $this->load->helper(array('form'));
        $data['submit_location'] = 'admin/people/actions/';

        $this->load->view('admin/header');
        $this->load->view('admin/people_index', $data);
        $this->load->view('admin/footer');
    }

    public function view($id) {
        if (!Person_model::dataExists($id))
            redirect('people/index');

        $person = new Person_model($id);

        $related = array();
        $connections = $person->getConnections();
        $i = 0;
        foreach ($connections as $conn) {
            $related[$i]['connection'] = $conn;
            $related[$i]['location'] = $conn->getPlace();
            ++$i;
        }

        $data['person'] = $person;
        $data['places'] = $related;
        $data['id'] = $id;

        $this->load->view('admin/header');
        $this->load->view('admin/people_view', $data);
        $this->load->view('admin/footer');
    }

    public function edit($id) {
        if (!Person_model::dataExists($id))
            redirect('people/index');

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $person = new Person_model($id);

        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('personBirthdate', 'Birthdate', 'callback_birthdate_check');
            $this->form_validation->set_error_delimiters('<p class="error">', '</p>');

            if ($this->form_validation->run()) { // form validated
                // Manage changes to person
                if ($this->input->post('personFName') != $person->getFName())
                    $person->setFName( $this->input->post('personFName') );

                if ($this->input->post('personLName') != $person->getLName())
                    $person->setLName( $this->input->post('personLName') );

                if ($this->input->post('personMName') != $person->getMName())
                    $person->setMName( $this->input->post('personMName') );

                if ($this->input->post('personMaName') != $person->getMaName())
                    $person->setMaName( $this->input->post('personMaName') );

                if ($this->input->post('personFName') != $person->getFName())
                    $person->setFName( $this->input->post('personFName') );

                if (($adopted = $this->input->post('personAdopted')) !== false && $adopted != $person->getAdopted())
                    $person->setAdopted($adopted);

                if (($gender = $this->input->post('personGender')) !== false && $gender != $person->getGender())
                    $person->setGender($gender);

                if ($this->input->post('personRace') != $person->getRace())
                    $person->setRace( $this->input->post('personRace') );

                if (strtotime($this->input->post('personBirthdate')) != $person->getBirthdate())
                    $person->setBirthdate( strtotime($this->input->post('personBirthdate')) );

                $person->save();
                $this->session->set_flashdata('success', true);
            } else {
                $this->session->set_flashdata('failure', true);
            }
        }
        $data['submit_location'] = 'admin/people/edit/'.$id;
        $data['person'] = $person;
        $this->load->view('admin/header');
        $this->load->view('admin/people_edit', $data);
        $this->load->view('admin/footer');
    }

    public function editConnection($id) {
        $this->load->model('Ptp_bridge_model');
        $this->load->model('Place_model');

        if (!Ptp_bridge_model::dataExists($id))
            redirect('people/index');

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data['connection'] = $connection = new Ptp_bridge_model($id);
        $data['person'] = $person = $connection->getPerson();
        $data['place'] = $place = $connection->getPlace();

        if ($this->input->post('submit')) {
            // we have nothing as far as validation right now, so we bypass it
            if (true || $this->form_validation->run()) {
                if ($this->input->post('associationId') != $connection->getAssociationId())
                    $connection->setAssociationId( $this->input->post('associationId') );

                if (($this->input->post('otherText') != $connection->getOtherText()) && ($this->input->post('associationId') == 0))
                    $connection->setOtherText( $this->input->post('otherText') );

                if ($this->input->post('startDate') != $connection->getStartDate())
                    $connection->setStartDate( strtotime($this->input->post('startDate')) );

                if ($this->input->post('endDate') != $connection->getEndDate())
                    $connection->setEndDate( strtotime($this->input->post('endDate')) );

                $connection->save();
                
                $this->session->set_flashdata('success', true);
            } else {
                $this->session->set_flashdata('failure', true);
            }
        }

        $data['submit_location'] = 'admin/people/editConnection/'.$id;
        $data['associationTypes'] = Ptp_bridge_model::getAssociationTypes();

        $this->load->view('admin/header');
        $this->load->view('admin/connection_edit', $data);
        $this->load->view('admin/footer');
    }

    public function create() {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $person = new Person_model();

        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('personBirthdate', 'Birthdate', 'callback_birthdate_check');
            $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
            if ($this->input->post('personAdopted') == 1)
                $this->form_validation->set_rules('orphName', 'Orphanage Name', 'required');

            if ($this->form_validation->run()) {

                if ($this->input->post('personFName'))
                    $person->setFName( $this->input->post('personFName') );

                if ($this->input->post('personLName'))
                    $person->setLName( $this->input->post('personLName') );

                if ($this->input->post('personMName'))
                    $person->setMName( $this->input->post('personMName') );

                if ($this->input->post('personMaName'))
                    $person->setMaName( $this->input->post('personMaName') );

                $person->setActive( ($this->input->post('personActive')) ? $this->input->post('personActive') : 0 );
                $person->setGender( ($this->input->post('personGender')) ? $this->input->post('personGender') : 'female' );
                $person->setRace( ($this->input->post('personRace')) ? $this->input->post('personRace') : 1);
                $person->setAdopted( ($this->input->post('personAdopted')) ? $this->input->post('personAdopted') : 0);

                $person->setType('user');

                if (strtotime($this->input->post('personBirthdate')))
                    $person->setBirthdate( strtotime($this->input->post('personBirthdate')) );

                $person->save();

                // You were adopted, and your parents don't love you
                if ($person->getAdopted() == 1) {
                    // We'll be needing these models
                    $this->load->model('Ptp_bridge_model');
                    $this->load->model('Place_model');
                    if ($this->input->post('personPlaceId')) {
                        // Was the place selected from the autofill form?
                        // Use that one then.
                        $place = new Place_model($this->input->post('personPlaceId'));
                        $bridge = $person->connectPlace($place);
                    } else {
                        $place = new Place_model();
                        
                        // create a new place
                        if ($this->input->post('orphName'))
                            $place->setName($this->input->post('orphName'));

                        if ($this->input->post('orphAddress1'))
                            $place->setAddress1($this->input->post('orphAddress1'));

                        if ($this->input->post('orphAddress2'))
                            $place->setAddress2($this->input->post('orphAddress2'));

                        if ($this->input->post('orphCity'))
                            $place->setCity($this->input->post('orphCity'));

                        if ($this->input->post('orphZipcode'))
                            $place->setZipcode($this->input->post('orphZipcode'));

                        if ($this->input->post('orphCountryId'))
                            $place->setCountryId($this->input->post('orphCountryId'));

                        // It is an orphanage
                        $place->setPlaceTypeId(16);

                        // Never forget
                        $place->save();

                        $bridge = $person->connectPlace($place);
                    }

                    // The date range is pretty important when it comes to this type of thing
                    if ($this->input->post('orphStartDate'))
                        $bridge->setStartDate($this->input->post('orphStartDate'));
                    if ($this->input->post('orphEndDate'))
                        $bridge->setEndDate($this->input->post('orphEndDate'));

                    // Let it be known, you were a resident of an orphanage
                    $bridge->setAssociationId(4);

                    $bridge->save();
                }


                $this->session->set_flashdata('success', true);
            } else {
                $this->session->set_flashdata('failure', true);
            }
        }

        $data['submit_location'] = 'admin/people/create';
        $this->load->view('admin/header');
        $this->load->view('admin/people_new', $data);
        $this->load->view('admin/footer');
    }

    public function archive($id) {
        if (!Person::dataExists($id))
            redirect('people/index');
        $person = new Person_model($id);
        $person->setType(0);
        $person->save();

        redirect('people/index');
    }

    public function delete($id) {
        if (!Person::dataExists($id))
            redirect('people/index');

        Person_model::deleteData($id);

        redirect('people/index');
    }

    public function birthdate_check($str) {
        if ($str != null && !strtotime($str)) {
            $this->form_validation->set_messsage('birthdate', 'The %s field is not a valid date');
            return false;
        } else {
            return true;
        }
    }

}
