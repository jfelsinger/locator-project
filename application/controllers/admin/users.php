<?php
class Users extends CI_Controller{
    public function __construct() {
        parent::__construct(); 
        $this->load->model('User_model');
    }

    public function index($sort=false) {
        $where = '1=1';
        $sortBy = 'id';

        if ($this->input->post('submitFilter')) {
            if (in_array($this->input->post('sort'), User_model::$columnNames))
                $sortBy = $this->input->post('sort');

            if ($this->input->post('filter')) {
                $whereArray = array();
                $filter = '\'%'.$this->db->escape_like_str($this->input->post('filter')).'%\'';
                $whereArray[] = 'username LIKE '.$filter;
                $whereArray[] = 'password LIKE '.$filter;
                $whereArray[] = 'lastModified LIKE '.$filter;
                // $whereArray[] = 'type LIKE '.$filter;
                $whereArray[] = 'email LIKE '.$filter;
                $whereArray[] = 'rating LIKE '.$filter;
                $where = '( '.implode(' OR ', $whereArray).' ) AND '.$where;
            }
        }

        $data['userCount'] = User_model::getCount($where);
        $data['noteCount'] = 12; #Note_model::getCount();
        $data['users'] = User_model::getMany(0, null, $sortBy, $where);

        $this->load->view('admin/header');
        $this->load->view('admin/user_index', $data);
        $this->load->view('admin/footer');
    }

    public function view($id) {
        if (!User_model::dataExists($id))
            redirect('people/index');

        $user = new User_model($id);

        $data['user'] = $user;
        $data['id'] = $id;

        $this->load->view('admin/header');
        $this->load->view('admin/user_view', $data);
        $this->load->view('admin/footer');
    }

    public function create() {
        $this->load->view('admin/header');
        $this->load->view('admin/user_create');
        $this->load->view('admin/footer');
    }

    public function archive($id) {
    }

    public function delete($id) {
    }
}
?>
