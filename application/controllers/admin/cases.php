<?php
class Cases extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('Case_model');
        $this->load->model('Person_model');
    }

    public function actions() {
        $cases = array();
        foreach ($this->input->post('cases') as $c)
            $cases[] = new Case_model($c);

        $this->session->set_flashdata('success', true);
        switch ($this->input->post('actions')) {
            case 'review':
                $this->session->set_flashdata('success', true);
                redirect('admin/cases/index');
                break;

            case 'archive':
                $this->session->set_flashdata('success', true);
                redirect('admin/cases/index');
                break;

            case 'delete':
                $this->session->set_flashdata('success', true);
                redirect('admin/cases/index');
                break;

            default:
                $this->session->set_flashdata('failure', true);
                redirect('admin/cases/index');
        }
    }

    public function index() {
        $sortBy = 'id';
        $where = "status<>'archived'";

        if ($this->input->post('submitFilter')) {
            if (in_array($this->input->post('sort'), Case_model::$columnNames))
                $sortBy = $this->input->post('sort');

            if ($this->input->post('filter')) {
                $whereArray = array();
                $filter = '\'%'.$this->db->escape_like_str($this->input->post('filter')).'%\'';
                $whereArray[] = 'number LIKE '.$filter;
                $whereArray[] = 'name LIKE '.$filter;
                $whereArray[] = 'status LIKE '.$filter;
                $whereArray[] = 'startDate LIKE '.$filter;
                $whereArray[] = 'endDate LIKE '.$filter;
                $whereArray[] = 'lastModified LIKE '.$filter;
                $whereArray[] = 'description LIKE '.$filter;
                $where = '( '.implode(' OR ', $whereArray).' ) AND '.$where;
            }
        }

        $this->load->library('pagination');
        $config['base_url'] = base_url().'admin/cases/index/';
        $config['total_rows'] = Case_model::getCount($where);
        $config['per_page'] = 35;
        $config['uri_segment'] = 4;

        $this->pagination->initialize($config);
        $page = $this->uri->segment($config['uri_segment']);
        $page = $page ? $page : 1;

        $data['caseCount'] = Case_model::getCount($where);
        $data['noteCount'] = 12; #Note_model::getCount();
        $data['cases'] = Case_model::getMany(0, null, $sortBy, $where);

        $this->load->helper(array('form'));
        $data['submit_location'] = 'admin/cases/actions/';

        $this->load->view('admin/header');
        $this->load->view('admin/cases_index', $data);
        $this->load->view('admin/footer');
    }

    public function view($id=false) {
        if ($id==false || !Case_model::dataExists($id))
            redirect('admin/cases/index');

        $case = new Case_model($id);
        $target = new Person_model($case->getTargetId());

        $header['title'] = 'Case '.$id;

        $data['case'] = $case;
        $data['target'] = $target;
        $data['leadCount'] = 12;
        $data['noteCount'] = 8;
        $data['startedBy'] = 'Admin 32';
        $data['id'] = $id;

        $this->load->view('admin/header', $header);
        $this->load->view('admin/cases_view', $data);
        $this->load->view('admin/footer');
    }

    public function edit($id=false) {
        if ($id==false || !Case_model::dataExists($id))
            redirect('admin/cases/index');

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $case = new Case_model($id);
        $person = $case->getTargetPerson();

        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('caseNumber', 'Case Number', 'required');
            $this->form_validation->set_rules('personBirthdate', 'Birthdate', 'callback_birthdate_check');
            $this->form_validation->set_error_delimiters('<p class="error">', '</p>');

            if ($this->form_validation->run()) { // form validated
                // Manage changes to person

                /* Person is a seperate entity, handle elsewhere
                if ($this->input->post('personFName') != $person->getFName())
                    $person->setFName( $this->input->post('personFName') );

                if ($this->input->post('personLName') != $person->getLName())
                    $person->setLName( $this->input->post('personLName') );

                if ($this->input->post('personMName') != $person->getMName())
                    $person->setMName( $this->input->post('personMName') );

                if ($this->input->post('personMaName') != $person->getMaName())
                    $person->setMaName( $this->input->post('personMaName') );

                if ($this->input->post('personFName') != $person->getFName())
                    $person->setFName( $this->input->post('personFName') );

                if (($adopted = $this->input->post('personAdopted')) !== false && $adopted != $person->getAdopted())
                    $person->setAdopted($adopted);

                if (($gender = $this->input->post('personGender')) !== false && $gender != $person->getGender())
                    $person->setGender($gender);

                if ($this->input->post('personRace') != $person->getRace())
                    $person->setRace( $this->input->post('personRace') );

                if (strtotime($this->input->post('personBirthdate')) != $person->getBirthdate())
                    $person->setBirthdate( strtotime($this->input->post('personBirthdate')) );

                $person->save();
                */

                // Manage changes to case 
                if ($this->input->post('caseNumber') != $case->getNumber())
                    $case->setNumber( $this->input->post('caseNumber') );

                if ($this->input->post('caseName') != $case->getName())
                    $case->setName( $this->input->post('caseName') );

                if ($this->input->post('casePriority') != $case->getStatus())
                    $case->setStatus( $this->input->post('casePriority') );

                if ($this->input->post('caseDescription') != $case->getDescription())
                    $case->setDescription( $this->input->post('caseDescription') );

                $case->save();
                redirect('admin/cases/view/'.$id);
            } else { // Invalid form data boo
            }
        }

        $data['submit_location'] = 'admin/cases/edit/'.$id;
        $data['case'] = $case;
        $data['person'] = $person;
        $this->load->view('admin/header');
        $this->load->view('admin/cases_edit', $data);
        $this->load->view('admin/footer');
    }

    public function create() {
        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        if ($this->input->post('submit')) { // if something was submitted
            $this->form_validation->set_rules('caseNumber', 'Case Number', 'required');
            $this->form_validation->set_rules('personBirthdate', 'Birthdate', 'callback_birthdate_check');

            if ($this->form_validation->run()) {
                $case = new Case_model();
                $person = new Person_model();

                if ($this->input->post('personFName'))
                    $person->setFName($this->input->post('personFName'));

                if ($this->input->post('personLName'))
                    $person->setLName($this->input->post('personLName'));

                if ($this->input->post('personMName'))
                    $person->setMName($this->input->post('personMName'));

                if ($this->input->post('personMaName'))
                    $person->setMaName($this->input->post('personMaName'));

                if (strtotime($this->input->post('personBirthdate')))
                    $person->setBirthdate($this->input->post('personBirthdate'));

                $person->setGender(($this->input->post('personGender'))?$this->input->post('personGender'):0);
                $person->setRace(($this->input->post('personRace'))?$this->input->post('personRace'):1);
                $person->setAdopted(($this->input->post('personAdopted'))?1:0);
                $person->setType('target');

                $person->save();
                
                if ($person->getAdopted() == 1) {
                    $this->load->model('Ptp_bridge_model');
                    $this->load->model('Place_model');
                    if ($this->input->post('personPlaceId')) {
                        $place = new Place_model($this->input->post('personPlaceId'));
                        $bridge = $person->connectPlace($place);
                    } else {
                        $place = new Place_model();
                        
                        // create a new place
                        if ($this->input->post('orphName'))
                            $place->setName($this->input->post('orphName'));

                        if ($this->input->post('orphAddress1'))
                            $place->setAddress1($this->input->post('orphAddress1'));

                        if ($this->input->post('orphAddress2'))
                            $place->setAddress2($this->input->post('orphAddress2'));

                        if ($this->input->post('orphCity'))
                            $place->setCity($this->input->post('orphCity'));

                        if ($this->input->post('orphZipcode'))
                            $place->setZipcode($this->input->post('orphZipcode'));

                        if ($this->input->post('orphCountryId'))
                            $place->setCountryId($this->input->post('orphCountryId'));

                        $place->setPlaceTypeId(16);

                        $place->save();

                        $bridge = $person->connectPlace($place);
                    }

                    if ($this->input->post('orphStartDate'))
                        $bridge->setStartDate($this->input->post('orphStartDate'));
                    if ($this->input->post('orphEndDate'))
                        $bridge->setEndDate($this->input->post('orphEndDate'));

                    $bridge->setAssociationId(4);

                    $bridge->save();
                }


                // Manage new case properties
                $case->setTargetId($person->getId());
                $case->setNumber($this->input->post('caseNumber'));
                $case->setSeekerId('1');

                if ($this->input->post('caseName'))
                    $case->setName($this->input->post('caseName'));

                $case->setStatus(($this->input->post('casePriority'))?$this->input->post('casePriority'):'low');

                if ($this->input->post('caseDescription'))
                    $case->setDescription($this->input->post('caseDescription'));

                $case->save();

                redirect('admin/cases/view/'.$id);
            } 
        }

        $data['submit_location'] = 'admin/cases/create';
        $this->load->view('admin/header');
        $this->load->view('admin/cases_new', $data);
        $this->load->view('admin/footer');
    }

    public function archive($id=false) {
        if ($this->input->post('cases')) {
            $cases = $this->input->post('cases');
            foreach ($cases as $case) {
                $case = new Case_model($id);
                $case->setStatus('archived');
                $case->save();
            }
        } elseif ($id!=false && Case_model::dataExists($id)) {
            $case = new Case_model($id);
            $case->setStatus('archived');
            $case->save();
        }

        redirect('admin/cases/view/'.$id);
    }

    public function delete($id=false) {
        if ($this->input->post('cases')) {
            $cases = $this->input->post('cases');
            foreach ($cases as $case)
                Case_model::deleteData($case);
        } elseif ($id!=false && Case_model::dataExists($id)) {
            Case_model::deleteData($id);
        }

        redirect('admin/cases/index');
    }

    public function mark($id=false) {
        redirect('admins/cases/index');
    }

    public function birthdate_check($str) {
        if ($str != null && !strtotime($str)) {
            $this->form_validation->set_message('birthdate_check', 'The %s field is not a valid date');
            return false;
        } else {
            return true;
        }
    }
}
