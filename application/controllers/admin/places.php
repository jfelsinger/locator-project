<?php
class Places extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('Place_model');
    }

    public function actions() {
        $places = array();
        foreach ($this->input->post('places') as $p)
            $places[] = new Place_model($p);

        $this->session->set_flashdata('success', true);
        switch ($this->input->post('actions')) {
            case 'review':
                $this->session->set_flashdata('success', true);
                redirect('admin/places/index');
                break;

            case 'archive':
                $this->session->set_flashdata('success', true);
                redirect('admin/places/index');
                break;

            case 'delete':
                $this->session->set_flashdata('success', true);
                redirect('admin/places/index');
                break;

            default:
                $this->session->set_flashdata('failure', true);
                redirect('admin/places/index');
        }
    }

    public function index() {
        $where = false;
        $sortBy = 'id';

        if ($this->input->post('submitFilter')) {
            if (in_array($this->input->post('sort'), Place_model::$columnNames))
                $sortBy = $this->input->post('sort');

            if ($this->input->post('filter')) {
                $whereArray = array();
                $filter = '\'%'.$this->db->escape_like_str($this->input->post('filter')).'%\'';
                $whereArray[] = 'name LIKE '.$filter;
                $whereArray[] = 'address1 LIKE '.$filter;
                $whereArray[] = 'address2 LIKE '.$filter;
                $whereArray[] = 'city LIKE '.$filter;
                $whereArray[] = 'region LIKE '.$filter;
                $whereArray[] = 'zipcode LIKE '.$filter;
                $whereArray[] = 'otherText LIKE '.$filter;
                $whereArray[] = 'description LIKE '.$filter;
                $whereArray[] = 'createdDate LIKE '.$filter;
                $where = '( '.implode(' OR ', $whereArray).' ) AND '.$where;
            }
        }

        $this->load->library('pagination');
        $config['base_url'] = base_url().'admin/places/index/';
        $data['placeCount'] = $config['total_rows'] = Place_model::getCount($where);
        $config['per_page'] = 35;
        $config['uri_segment'] = 4;

        $this->pagination->initialize($config);
        $page = $this->uri->segment($config['uri_segment']);
        $page = $page ? $page : 1;

        $data['noteCount'] = 24; #Note_model::GetCount();
        $data['places'] = Place_model::getMany($page, $config['per_page']+1, $sortBy, $where);

        $this->load->helper(array('form'));
        $data['submit_location'] = 'admin/people/actions/';

        $this->load->view('admin/header');
        $this->load->view('admin/places_index', $data);
        $this->load->view('admin/footer');
    }

    public function view($id) {
        if (!Place_model::dataExists($id))
            redirect('people/index');

        $place = new Place_model($id);

        $related = array();
        $connections = $place->getConnections();
        $i = 0;
        foreach ($connections as $conn) {
            $related[$i]['connection'] = $conn;
            $related[$i]['person'] = $conn->getPerson();
        }

        $data['place'] = $place;
        $data['related'] = $related;
        $data['id'] = $id;

        $this->load->view('admin/header');
        $this->load->view('admin/places_view', $data);
        $this->load->view('admin/footer');
    }

    public function edit($id) {
        if (!Place_model::dataExists($id))
            redirect('people/index');

        $this->load->library('form_validation');

        $place = new Place_model($id);

        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('placeName', 'Name', 'required');
            $this->form_validation->set_rules('address1', 'Address', 'required');
            $this->form_validation->set_rules('city', 'City', 'required');
            $this->form_validation->set_rules('region', 'Region', 'required');
            $this->form_validation->set_rules('zipcode', 'Zipcode', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');
            $this->form_validation->set_rules('placeType', 'Place Type', 'required');
            if ($this->input->post('placeType') == 0)
                $this->form_validation->set_rules('otherText', 'Other Text', 'required');

            $this->form_validation->set_error_delimiters('<p class="error">', '</p>');

            if ($this->form_validation->run()) {
                $place->setName($this->input->post('placeName'));

                $place->setAddress1($this->input->post('address1'));
                if ($this->input->post('address2'))
                    $place->setAddress2($this->input->post('address2'));

                $place->setCity($this->input->post('city'));
                $place->setRegion($this->input->post('region'));
                $place->setZipcode($this->input->post('zipcode'));
                $place->setCountryId($this->input->post('country'));

                if ($this->input->post('placeType') != 0)
                    $place->setPlaceTypeId($this->input->post('placeType'));
                else {
                    $place->setOtherText($this->input->post('other'));
                    $place->setPlaceTypeId(0);
                }

                if ($this->input->post('description'))
                    $place->setDescription($this->input->post('description'));

                $place->save();
                $data['success'] = true;
            } else {
                $data['success'] = false;
            }
        }

        $data['place'] = $place;
        $data['countries'] = Place_model::getCountries();
        $data['placeTypes'] = Place_model::getTypes();
        $data['submit_location'] = 'admin/places/edit/'.$id;
        $this->load->view('admin/header');
        $this->load->view('admin/places_edit', $data);
        $this->load->view('admin/footer');
    }

    public function editConnection($id) {
        $this->load->model('Ptp_bridge_model');
        $this->load->model('Person_model');

        if (!Ptp_bridge_model::dataExists($id))
            redirect('places/index');

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $data['connection'] = $connection = new Ptp_bridge_model($id);
        $data['person'] = $person = $connection->getPerson();
        $data['place'] = $place = $connection->getPlace();

        if ($this->input->post('submit')) {

            if (true || $this->form_validation_run()) {
                if ($this->input->post('associationId') != $connection->getAssociationId())
                    $connection->setAssociationId( $this->input->post('associationId') );

                if (($this->input->post('otherText') != $connection->getOtherText()) && ($this->input->post('associationId') == 0))
                    $connection->setOtherText( $this->input->post('otherText') );

                if ($this->input->post('startDate') != $connection->getStartDate())
                    $connection->setStartDate( strtotime($this->input->post('startDate')) );

                if ($this->input->post('endDate') != $connection->getEndDate())
                    $connection->setEndDate( strtotime($this->input->post('endDate')) );

                $connection->save();
                $data['success'] = true;
            } else {
                $data['success'] = false;
            }
        }

        $data['submit_location'] = 'admin/places/editConnection/'.$id;
        $data['associationTypes'] = Ptp_bridge_model::getAssociationTypes();

        $this->load->view('admin/header');
        $this->load->view('admin/connection_edit', $data);
        $this->load->view('admin/footer');
    }

    public function create() {
        $this->load->library('form_validation');

        $place = new Place_model();

        if ($this->input->post('submit')) {
            $this->form_validation->set_rules('placeName','address1','city','region','zipcode','country','placeType');
            if ($this->input->post('placeType') == "other")
                $this->form_validation->set_rules('other');

            $this->form_validation->set_error_delimiters('<p class="error">', '</p>');

            if ($this->form_validation->run()) {
                $place->setName($this->input->post('placeName'));

                $place->setAddress1($this->input->post('address1'));
                if ($this->input->post('address2'))
                    $place->setAddress2($this->input->post('address2'));

                $place->setCity($this->input->post('city'));
                $place->setRegion($this->input->post('region'));
                $place->setZipcode($this->input->post('zipcode'));
                $place->setCountryId($this->input->post('Country'));

                if ($this->input->post('placeType') != "other")
                    $place->setPlaceTypeId($this->input->post('placeType'));
                else {
                    $place->setOtherText($this->input->post('other'));
                    $place->setPlaceTypeId(0);
                }

                if ($this->input->post('description'))
                    $place->setDescription($this->input->post('description'));

                $place->save();
                $data['success'] = true;
            } else {
                $data['success'] = false;
            }
        }

        $data['countries'] = Place_model::getCountries();
        $data['placeTypes'] = Place_model::getTypes();
        $data['submit_location'] = 'admin/places/create';
        $this->load->view('admin/header');
        $this->load->view('admin/places_new', $data);
        $this->load->view('admin/footer');
    }

    public function archive($id) {
    }

    public function delete($id) {
    }
}
?>
