<?php
class Notifications extends ci_controller {
    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('header');
        $this->load->view('locator/notifications/list');
        $this->load->view('footer');
    }

    public function view($id) {
        $this->load->view('header');
        $this->load->view('locator/notifications/view');
        $this->load->view('footer');
    }
}
?>


