<?php
class Education extends Secure_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Person_model');
        $this->load->model('Place_model');
        $this->load->model('Ptp_bridge_model');
    }

    public function index() {
        $user = $this->ion_auth->user()->row();
        $person = new Person_model($user->personId);
        $bridges = $person->getConnections();

        foreach ($bridges as $key => $bridge) {
            if (!in_array($bridge->getPlace()->getPlaceTypeId(), array(1, 3, 4, 5)))
                unset($bridges[$key]);
        }

        $data['bridges'] = $bridges;
        $data['sidebar'] = $this->load->view('locator/sidebar',null, true);

        $this->load->view('header');
        $this->load->view('locator/education/list', $data);
        $this->load->view('footer');
    }

    public function remove($id) {
        $bridge = new Ptp_bridge_model($id);
        if ($bridge->getPersonId == $user->personId)
            $bridge->remove(); // if the bridge belongs to the person, remove it
    }

    public function edit($id) {
        $data['sidebar'] = $this->load->view('locator/sidebar',null, true);

        $this->load->view('header');
        $this->load->view('locator/education/edit', $data);
        $this->load->view('footer');
    }

    public function add() {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<p class="error">','</p>');

        if ($this->input->post('submit')) { // if something was submitted
            $this->form_validation->set_rules('dateFrom', 'Date From', 'required');

            $person = $this->ion_auth->user()->row();
            $person = new Person_model($person->personId);

            if ($this->input->post('placeId')) {
                if ($this->form_validation->run()) {
                    $place = new Place_model($this->input->post('placeId'));
                    $bridge = $person->connectPlace($place);

                    $bridge = $person->connectPlace($place);

                    if ($this->input->post('dateFrom'))
                        $bridge->setStartDate(strtotime($this->input->post('dateFrom')));
                    if ($this->input->post('dateTo'))
                        $bridge->setEndDate(strtotime($this->input->post('dateTo')));

                    $bridge->setAssociationId(1);

                    $bridge->save();

                    $this->session->set_flashdata('message', 'School successfully added to profile');
                    redirect(base_url() . 'user/education/index');
                }
            } else {
                $this->form_validation->set_rules('placeName', 'School Name', 'required');
                $this->form_validation->set_rules('placeCountry', 'Country', 'required');
                $this->form_validation->set_rules('placeCity', 'City', 'required');
                $this->form_validation->set_rules('placeRegion', 'State/Region', 'required');

                if ($this->form_validation->run()) {
                    $place = new Place_model();

                    if ($this->input->post('placeName'))
                        $place->setName($this->input->post('placeName'));

                    if ($this->input->post('placeCity'))
                        $place->setCity($this->input->post('placeCity'));

                    if ($this->input->post('placeRegion'))
                        $place->setRegion($this->input->post('placeRegion'));

                    if ($this->input->post('placeCountry'))
                        $place->setCountryId($this->input->post('placeCountry'));

                    if ($this->input->post('placeType'))
                        $place->setType($this->input->post('placeType'));

                    $place->save();

                    $bridge = $person->connectPlace($place);

                    if ($this->input->post('dateFrom'))
                        $bridge->setStartDate(strtotime($this->input->post('dateFrom')));
                    if ($this->input->post('dateTo'))
                        $bridge->setEndDate(strtotime($this->input->post('dateTo')));

                    $bridge->setAssociationId(1);

                    $bridge->save();

                    $this->session->set_flashdata('message', 'School successfully added to profile');
                    redirect(base_url() . 'user/education/index');
                }
            }
        }


        $data['sidebar'] = $this->load->view('locator/sidebar',null, true);
        $data['countries'] = $this->locator->getCountries();

        $this->load->view('header');
        $this->load->view('locator/education/add', $data);
        $this->load->view('footer');
    }
}
?>
