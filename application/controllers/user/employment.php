<?php
class Employment extends ci_controller {
    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('header');
        $this->load->view('locator/employment/list');
        $this->load->view('footer');
    }

    public function edit($id) {
        $this->load->view('header');
        $this->load->view('locator/employment/edit');
        $this->load->view('footer');
    }

    public function add() {
        $this->load->view('header');
        $this->load->view('locator/employment/new');
        $this->load->view('footer');
    }
}
?>
