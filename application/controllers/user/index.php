<?php
//class Index extends Secure_Controller {
class Index extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Person_model');
        $this->load->model('Place_model');
        $this->load->model('Ptp_bridge_model');
    }

    public function index() {
        $data['sidebar'] = $this->load->view('locator/sidebar',null, true);

        $this->load->view('header');
        $this->load->view('locator/home', $data);
        $this->load->view('footer');
    }

}
?>
