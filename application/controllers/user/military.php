<?php
class Military extends ci_controller {
    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('header');
        $this->load->view('locator/military/list');
        $this->load->view('footer');
    }

    public function edit($id) {
        $this->load->view('header');
        $this->load->view('locator/military/edit');
        $this->load->view('footer');
    }

    public function add() {
        $this->load->view('header');
        $this->load->view('locator/military/new');
        $this->load->view('footer');
    }
}
?>

