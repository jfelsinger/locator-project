<?php
class Residency extends ci_controller {
    function __construct() {
        parent::__construct();
        $this->load->model('Ptp_bridge_model');
        $this->load->model('Place_model');
        $this->load->model('Person_model');
    }

    public function index() {
        if (!$this->ion_auth->logged_in())
            redirect(base_url() . 'login');

        $user = $this->ion_auth->user()->row();
        $person = new Person_model($user->personId);
        $connections = $person->getConnections();

        $places = array();

        foreach ($connections as $key => $connection) {
            if (in_array($connection->getAssociationId(), array(4, 6)))
                $places[] = $connection->getPlace();
        }


        $data['places'] = $places;
        $data['sidebar'] = $this->load->view('locator/sidebar',null, true);

        $this->load->view('header');
        $this->load->view('locator/residency/list');
        $this->load->view('footer');
    }

    public function edit($id) {
        $this->load->view('header');
        $this->load->view('locator/residency/edit');
        $this->load->view('footer');
    }

    public function add() {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<p class="error">','</p>');

        if ($this->input->post('submit')) { // if something was submitted
            $this->form_validation->set_rules('dateFrom', 'Date From', 'required');

            $person = $this->ion_auth->user()->row();
            $person = new Person_model($person->personId);
        }


        $this->load->view('header');
        $this->load->view('locator/residency/new');
        $this->load->view('footer');
    }
}
?>


