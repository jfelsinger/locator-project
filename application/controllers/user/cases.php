<?php
class Cases extends ci_controller {
    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('header');
        $this->load->view('locator/cases/list');
        $this->load->view('footer');
    }

    public function view($id) {
        $this->load->view('header');
        $this->load->view('locator/cases/view');
        $this->load->view('footer');
    } 

    public function edit($id) {
        $this->load->view('header');
        $this->load->view('locator/cases/edit');
        $this->load->view('footer');
    }

    public function add() {
        $this->load->view('header');
        $this->load->view('locator/cases/new');
        $this->load->view('footer');
    }
}
?>
