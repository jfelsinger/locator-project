<?php
class Faq extends CI_Controller {
    
    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('header');
        $this->load->view('faq');
        $this->load->view('footer');
    }
}
?>
