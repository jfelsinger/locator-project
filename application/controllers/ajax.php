<?php
class Ajax extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('place_model');
        $this->load->model('ptp_bridge_model');
        $this->load->model('person_model');
    }

    public function autocomplete($type=false) {
        $result = array();
        $text = $this->input->post('text');

        $type = trim($type);
        $type = strtolower($type);
        switch ($type) {
            case 'country':
                $result = $this->acCountry($text);
                break;
            case 'place':
                $placeType = $this->input->post('type');
                $result = $this->acPlace($text, $placeType);
                break;
        }
        echo json_encode($result);
    }

    private function acCountry($text=false) {
        $result=array();
        if ($text && is_string($text)) {
            $this->load->model('Place_model');
            $countries = Place_model::getCountries();
            foreach ($countries as $country)
                $result[] = (object) array(
                    'label'     => $country['name'],
                    'id'        => $country['id']
                );
        }
        return $result;
    }

    private function acPlace($text=false, $type=false) {
        $result = array();
        if ($text && is_string($text)) {
            $text = "'%".$this->db->escape_like_str($text)."%'";

            $join = array();
            $join[] = "LEFT JOIN countries c";
            $join[] = "ON places.countryId = c.id";
            $join[] = "LEFT JOIN countrynames cn";
            $join[] = "ON cn.countryId = c.id";
            $join[] = "LEFT JOIN placetypes pt";
            $join[] = "ON pt.id = places.PlaceTypeId";
            $join = implode(' ', $join);

            $where = array();
            $where[] = "(places.name LIKE ".$text;
            $where[] = "address1 LIKE ".$text;
            $where[] = "address2 LIKE ".$text;
            $where[] = "city LIKE ".$text;
            $where[] = "region LIKE ".$text;
            $where[] = "description LIKE ".$text;
            $where[] = "zipcode LIKE ".$text;
            $where[] = "c.isoAlpha2 LIKE ".$text;
            $where[] = "c.isoAlpha3 LIKE ".$text;
            $where[] = "cn.name LIKE ".$text.")";
            $where = implode(' OR ', $where);

            // if ($type)
                switch ($type)
                {
                    case 'school':
                        $where .= "AND pt.id IN (1, 3, 4, 5, 0)";
                        break;
                    default:
                        $where .= "AND pt.type=" . $this->db->escape($type);
                }

            $places = Place_model::getMany(0, 14, 'id', $where, $join);

            foreach ($places as $place) {
                $result[] = (object)array(
                    'label'     => $place->getName(),
                    'id'        => $place->getId(),
                    'name'      => $place->getName(),
                    'address1'  => $place->getAddress1(),
                    'address2'  => $place->getAddress2(),
                    'city'      => $place->getCity(),
                    'region'    => $place->getRegion(),
                    'zipcode'   => $place->getZipcode(),
                    'country'   => $place->getCountry(),
                    'countryId' => $place->getCountryId()
                );
            }
        }
        return $result;
    }
}
?>


