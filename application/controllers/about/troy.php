<?php
class Troy extends CI_Controller {
    
    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->view('header');
        $this->load->view('about/troy');
        $this->load->view('footer');
    }
}
?>
