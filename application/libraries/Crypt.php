<?php if ( !defined('BASEPATH')) exit('No direct script access allowed'); 

/*
 * This is currently using md5.
 * "Why?" you ask.
 * Because, I know it is going to work this way,
 * at least untill we get it into the actual environment.
 *
 */

class Crypt {

    private static $cost = '';
    private static $algorithm = '$1$';
    private static $salt_length = 8;

    private static $salt = '477b3949c4z88ft477b3949cz488ftpp477b3949c488f0tp';

    private static function unique_salt() {
        return substr(sha1(mt_rand()), 0, self::$salt_length);
    }

    public function hash($password) {

        $conf = self::$algorithm . self::$cost . self::unique_salt() . '$';


        return crypt($password . self::$salt, $conf);
    }

    public function check($hash, $password) {
        $length = strlen(self::$cost) + strlen(self::$algorithm) + self::$salt_length;

        $full_salt = substr($hash, 0, $length) . '$';
        $new_hash = crypt($password . self::$salt, $full_salt);

        if ($hash == $new_hash)
            return true;
        return false;
    }
}
?>
