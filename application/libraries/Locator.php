<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Locator
{
    public function getRaces() {
        $CI = get_instance();
        $sql = "SELECT raceId as value, name FROM races";
        $query = $CI->db->query($sql);
        return $query->result_array();
    }

    public function getCountries() {
        $CI = get_instance();
        $sql = "SELECT c.id as value, cn.name  FROM countries c LEFT JOIN countrynames cn ON c.id=cn.countryId WHERE languageId='EN'";
        $query = $CI->db->query($sql);
        return $query->result_array();
    }
}
