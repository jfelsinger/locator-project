<?php

class Case_model extends MY_Model {
    const TABLE_NAME = 'cases';

    protected
        $id,
        $number,
        $name,
        $targetPerson,
        $targetId,
        $seekerUser,
        $seekerId,
        $status,
        $startDate,
        $endDate,
        $description,
        $lastModified;

    protected
        $private = array('id');

    static
        $columnNames = array(
			'id' => 'id',
			'number' => 'number',
			'name' => 'name',
			'targetId' => 'targetPersonId',
			'seekerId' => 'seekerUserId',
			'status' => 'status',
			'startDate' => 'startDate',
			'endDate' => 'endDate',
            'description' => 'description',
			'lastModified' => 'lastModified',
        );

    public function __construct($id=false) {
        if ($id !== false) {
            $sql = "SELECT
                        c.id,
                        c.number,
                        c.name,
                        c.targetPersonId,
                        c.seekerUserId,
                        c.status,
                        c.description,
                        UNIX_TIMESTAMP(c.startDate) as startDate,
                        UNIX_TIMESTAMP(c.endDate) as endDate,
                        UNIX_TIMESTAMP(c.lastModified) as lastModified
                    FROM
                        cases c
                    WHERE
                        c.id='{$id}'
                    LIMIT 1";
            $query = $this->db->query($sql);
            $result = $query->row_array();

            $this->id               = $result['id'];
            $this->number           = $result['number'];
            $this->name             = $result['name'];
            $this->targetId         = $result['targetPersonId'];
            $this->seekerId         = $result['seekerUserId'];
            $this->status           = $result['status'];
            $this->startDate        = $result['startDate'];
            $this->endDate          = $result['endDate'];
            $this->lastModified     = $result['lastModified'];
            $this->description      = $result['description'];
        }
    }

    public function save($keyNames = false) {
        if ($keyNames === false)
            $keyNames = array();

        if (!$this->startDate)
            $this->setStartDate(time());

        if (in_array('startDate', $this->changed))
            $keyNames['startDate'] = "FROM_UNIXTIME({$this->startDate})";

        if (in_array('lastModified', $this->changed))
            $keyNames['lastModified'] = "FROM_UNIXTIME({$this->lastModified})";

        if (in_array('endDate', $this->changed))
            $keyNames['endDate'] = "FROM_UNIXTIME({$this->endDate})";

        parent::save($keyNames);
    }

    public function getLeads($offset=0, $limit=null, $sort='id DESC') {
        return $this->getRelated('Lead', 'caseId', $offset, $limit, $sort);
    }

    public function getLeadCount() {
        return Lead::getCount("`caseId`={$this->id}");
    }

    public function getAlerts($offset=0, $limit=null, $sort='id DESC') {
        return $this->getRelated('Alert', 'caseId', $offset, $limit, $sort);
    }

    public function getAlertCount() {
        return Alert::getCount("`caseId`={$this->id}");
    }

    public function getComments($offset=0, $limit=null, $sort='id DESC') {
        return $this->getRelated('Comment', 'caseId', $offset, $limit, $sort);
    }

    public function makeComment($commentInfo) {
    }

    public function getReplies($offset=0, $limit=null, $sort='id DESC') {
        return $this->getRelated('Reply', 'caseId', $offset, $limit, $sort);
    }

    public function getReplyCount() {
        return Reply::getCount("`caseId`={$this->id}");
    }

    public function getTargetPerson() {
        if (!$this->targetPerson || ($this->targetId != $this->targetPerson->getId())) {
            $this->load->model('Person_model');
            $this->targetPerson = new Person_model($this->targetId);
        }
        return $this->targetPerson;
    }

    public function getSeekerUser() {
        if (!$this->seekerUser || ($this->seekerId != $this->seekerUser->getId()))
            $this->seekerUser = new User($this->seekerId);
        return $this->seekerUser;
    }

    public function getStartDate($timeFormat = false) {
        if ($timeFormat !== false)
            return strftime($timeFormat, $this->startDate);
        else
            return $this->startDate;
    }

    public function getEndDate($timeFormat = false) {
        if ($timeFormat !== false)
            return strftime($timeFormat, $this->endDate);
        else
            return $this->endDate;
    }

    public function getLastModified($timeFormat = false) {
        if ($timeFormat !== false)
            return strftime($timeFormat, $this->lastModified);
        else
            return $this->lastModified;
    }
}
