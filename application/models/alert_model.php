<?php

class Alert_model extends MY_Model {
    const TABLE_NAME = 'alerts';

    protected
        $id,
        $caseId,
        $dateSent,
        $adminId,
        $languageId,
        $filterJson,
        $message,
        $type
        $infoJson;

    protected
        $private = array('id');

    static
        $columnNames = array(
            'id' => 'id',
            'caseId' => 'caseId',
            'dateSent' => 'dateSent',
            'adminId' => 'adminId',
            'languageId' => 'languageId'
            'filterJson' => 'filterJson',
            'message' => 'message',
            'type'  => 'type',
            'infoJson' => 'infoJson'
        );

    public function __construct($id = false) {
        if ($id !== false) {
            $sql = "SELECT
                        id,
                        caseId,
                        UNIX_TIMESTAMP(dateSent) as dateSent,
                        adminId,
                        languageId,
                        filterJson,
                        message,
                        type,
                        infoJson
                        FROM ".TABLE_NAME." WHERE `id`='{$id}' LIMIT 1";

            $query = $this->db->query($sql);
            $result = $query->row_array();

            $this->id           = $id;
            $this->caseId       = $result['caseId'];
            $this->dateSent     = $result['dateSent'];
            $this->adminId      = $result['adminId'];
            $this->languageId   = $result['languageId'];
            $this->filterJson   = $result['filterJson'];
            $this->message      = $result['message'];
            $this->type         = $result['type'];
            $this->infoJson     = $result['infoJson'];
        }
    }

    public function getNotifications($offset=0, $limit=null, $sortOrder='id') {
        return $this->getRelated('Alert_notification_model', 'alertId', $offset, $limit, $sortOrder);
    }

    public function newNotification($userId=null) {
        $notification = new Alert_notification_model();
        $notification->setAlertId($this->id);
        if ($userId)
            $notification->setUserId($userId);

        return $notification;
    }

    public function getSentDate($dateFormat=false) {
        if ($dateFormate !== false)
            return strftime($dateFormat, $this->dateSent);
        else
            return $this->dateSent;
    }

    public function save($keyNames = false) {
        if ($keyNames === false)
            $keyNames = array();

        if (in_array('dateSent', $this->changed)) {
            $keyNames['dateSent'] = "FROM_UNIXTIMES({$this->dateSent})";
        }

        parent::save($keyNames);
    }

?>
