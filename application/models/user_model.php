<?php
class User_model extends MY_Model {
    const TABLE_NAME = 'users';

    protected
        $id,
        $username,
        $created_on,
        $personId,
        $email,
        $rating,
        $lastModified,
        $last_login,
        $active,
        $banned;

    protected
        $private = array('id', 'password');

    static
        $columnNames = array(
            'id' => 'id',
            'username' => 'username',
            'created_on' => 'created_on',
            'personId' => 'personId',
            'email' => 'email',
            'lastModified' => 'lastModified',
            'rating' => 'rating', 
            'password' => 'password',
            'last_login' => 'last_login',
            'active' => 'active',
            'banned' => 'banned',
        );

    public function __construct($id = false) {
        if ($id !== false) {
            $sql = "SELECT 
                        u.id, 
                        u.username, 
                        UNIX_TIMESTAMP(u.created_on) AS created_on, 
                        u.personId, 
                        u.email, 
                        u.rating, 
                        UNIX_TIMESTAMP(u.lastModified) AS lastModified,
                        UNIX_TIMESTAMP(u.last_login) AS last_login,
                        u.active,
                        u.banned
                    FROM users u LEFT JOIN usersettings us ON u.id=us.userId WHERE u.id='{$id}' LIMIT 1";

            $query = $this->db->query($sql);
            $result = $query->row_array();

            $this->id           = $id;
            $this->username     = $result['username'];
            $this->created_on   = $result['created_on'];
            $this->personId     = $result['personId'];
            $this->email        = $result['email'];
            $this->rating       = $result['rating'];    // Actually, calculated
            $this->lastModified = $result['lastModified'];
            $this->last_login   = $result['last_login'];
            $this->active       = $result['active'];
            $this->banned       = $result['banned'];
        }
    }

  // bbrown - 01-26-2013
  // Commenting out the following fuctions as most of this logic will be replaced with the Ion Auth library.
  //   public static function login($username, $password) {
  //       $username = $this->db->escape($username);
  //       $sql = "SELECT id, password FROM users WHERE `username`='{$username}' OR `email`='{$username}' LIMIT 1";
  //       $query = $this->db->query($sql);
  //       if ($query->num_rows()) {
  //           $this->load->library('crypt');
  //           $result = $query->row_array();
  //           $hash = $result['password'];
  //           if ($this->crypt->check($hash, $password)) {
  //               $user = new User($result['id']);
  //               $this->session->set_userdata(array('user' => $user));
  //               return true;
  //           }
  //       }
  //       return false;
  //   }

  //   public static function isLoggedIn() {
		// return ($this->session->userdata('user') instanceof User_model);
  //   }

  //   public static function hasPermission() {
  //       // checks to see if the user has permission to be on the current page,
  //       // or maybe something else? Lets decide later
  //   }

  //   public function logout() {
  //       $this->session->unset_userdata('user');
  //   }

  //   public function checkPassword($password, $hashed=true) {
  //       $this->load->library('crypt');
  //       if (!$hashed)
  //           $password = $this->crypt->hash($password);
  //       $sql = "SELECT id, password FROM users WHERE `username`='{$username}' LIMIT 1";
  //       $query = $this->db->query($sql);
  //       if ($query->num_rows()) {
  //           $result = $query->row_array();
  //           $hash = $result['password'];
  //           return $this->crypt->check($hash, $password);
  //       } else
  //           return false;
  //   }

    public function getJoinDate($dateFormat=false) {
        if ($dateFormat !== false)
            return strftime($dateFormat, $this->joinDate);
        else
            return $this->joinDate;
    }

    public function getLastModified($dateFormat=false) {
        if ($dateFormat !== false)
            return strftime($dateFormat, $this->lastModified);
        else
            return $this->lastModified;
    }

    public function getPerson() {
        return new Person($this->personId);
    }

    public function getRating() {
        // this is going to have to be calculated
        return $this->rating;
    }

    // public static function exists($username, $email=false) {
    //     $type = ($email)? 'email' : 'username';
    //     $username = $this->db->escape($username);
    //     $sql = "SELECT COUNT(*) AS c FROM users WHERE `{$type}`='{$username}'";
    //     $query = $this->db->query($sql);
    //     $result = $query->row();
    //     return ($result->c != 0);
    // }

    // public function save($password=false, $hashed=false) {
    //     $keyNames = array();

    //     if (isset($this->changed['lastModified']))
    //         $keyNames['lastModified'] = "FROM_UNIXTIME('{$this->lastModified}')";
    //     else
    //         $keyNames['lastModified'] = "FROM_UNIXTIME('" . time() . "')";

    //     if (isset($this->changed['joinDate']))
    //         $keyNames['joinDate'] = "FROM_UNIXTIME('{$this->joinDate}')";
    //     elseif (!$this->id)
    //         $keyNames['joinDate'] = "FROM_UNIXTIME('" . time() . "')";

    //     if ($password !== false) {
    //         $this->load->library('crypt');
    //         $keyNames['password'] = ($hashed)? "'$password'" : "'".$this->crypt->hash($password).'\'';
    //     }

    //     parent::save($keyNames);
    // }
}
?>
