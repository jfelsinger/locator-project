<?php

class Ptp_bridge_model extends MY_Model {
    const TABLE_NAME = 'peopletoplaces';
    protected
        $id,
        $personId,
        $placeId,
        $startDate,
        $endDate,
        $associationId,
        $otherText,
        $type;

    protected
        $private = array('id');

    static
        $columnNames = array(
            'id' => 'id',
            'personId' => 'personId',
            'placeId' => 'placeId',
            'startDate' => 'startDate',
            'endDate' => 'endDate',
            'associationId' => 'associationId',
            'otherText' => 'otherText',
            'type' => 'type'
        );

    function __construct($id=false) {
        if ($id !== false) {
            $sql = "SELECT
                        ptp.id,
                        ptp.personId,
                        ptp.placeId,
                        startDate,
                        endDate,
                        ptp.associationId,
                        ptp.otherText,
                        at.type
                    FROM
                        peopletoplaces ptp
                    LEFT JOIN
                        associationtypes at
                    ON
                        ptp.associationId=at.id
                    WHERE
                        ptp.id='{$id}'
                    LIMIT 1";

            $query = $this->db->query($sql);
            $result = $query->row_array();

            $this->id               = $id;
            $this->personId         = $result['personId'];
            $this->placeId          = $result['placeId'];
            $this->startDate        = strtotime($result['startDate']);
            $this->endDate          = strtotime($result['endDate']);
            $this->associationId    = $result['associationId'];
            $this->otherText        = $result['otherText'];
            $this->type             = $result['type'];
        }
    }

    public function getPerson() {
        $this->load->model('Person_model');
        return new Person_model($this->personId);
    }

    public function getPlace() {
        $this->load->model('Place_model');
        return new Place_model($this->placeId);
    }

    public function getStartDate($dateFormat=false) {
        if ($dateFormat === false)
            return $this->startDate;
        else
            return strftime($dateFormat, $this->startDate);
    }

    public function getEndDate($dateFormat=false) {
        if ($this->endDate != null) {
            if ($dateFormat === false)
                return $this->endDate;
            else
                return strftime($dateFormat, $this->endDate);
        } else {
            if ($dateFormat === false)
                return time();
            else
                return 'Present';
        }
    }

    public function getType() {
        if ($this->associationId == 0)
            return $this->otherText;
        else
            return $this->type;
    }

    public function save($keyNames=false) {
        if ($keyNames === false)
            $keyNames = array();

        if (in_array('startDate', $this->changed))
            $keyNames['startDate'] = "FROM_UNIXTIME({$this->startDate})";

        if (in_array('endDate', $this->changed))
            $keyNames['endDate'] = "FROM_UNIXTIME({$this->endDate})";

        parent::save($keyNames);
    }

    static public function getAssociationTypes($page=false) {
        $return = array();

        if ($page)
            $page = "WHERE `pageId`=$page";
        else
            $page = '';

        $sql = "SELECT
                    at.id, at.type
                FROM
                    associationtypes at
                $page";
        
        $query = static::$db->query($sql);
        foreach ($query->result_array() as $row)
            $return[] = $row;

        return $return;
    }
}
?>
