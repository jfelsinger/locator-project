<?php

class Person_model extends MY_Model {
    const TABLE_NAME = 'people';

    protected
        $id,
        $fName,
        $lName,
        $mName,
        $maName,
        $suffix,
        $gender,
        $birthdate,
        $race,
        $type,
        $adopted,
        $places = false,
        $active,
        $createdDate,
        $pictures = array();

    protected
        $private = array('id');

    static
        $columnNames = array(
            'id' => 'id',
            'fName' => 'fName',
            'lName' => 'lName',
            'mName' => 'mName',
            'maName' => 'maName',
            'suffix' => 'suffix',
            'gender' => 'gender',
            'birthdate' => 'birthdate',
            'adopted' => 'adopted',
            'type' => 'type',
            'race' => 'race',
            'active' => 'active',
            'createdDate' => 'createdDate'
        );

    public function __construct($id=false) {
        if ($id !== false) {
            $sql = "SELECT 
                        id,
                        fName,
                        lName,
                        mName,
                        maName,
                        suffix,
                        gender,
                        adopted,
                        active,
                        UNIX_TIMESTAMP(createdDate) as createdDate,
                        UNIX_TIMESTAMP(createdDate) as birthdate,
                        type,
                        race
                    FROM people WHERE `id`='{$id}' LIMIT 1";

            $query = $this->db->query($sql);
            $result = $query->row_array();

            $this->id           = $id;
            $this->fName        = $result['fName'];
            $this->lName        = $result['lName'];
            $this->mName        = $result['mName'];
            $this->maName       = $result['maName'];
            $this->suffix       = $result['suffix'];
            $this->gender       = $result['gender'];
            $this->birthdate    = strtotime($result['birthdate']);
            $this->type         = $result['type'];
            $this->race         = $result['race'];
            $this->adopted      = $result['adopted'];
            $this->createdDate  = $result['createdDate'];
            $this->active       = $result['active'];
        }
    }

    public function getPlaces($offset=0, $limit=null, $sortOrder='id') {
        // There has GOT to be a better way
        return $this->getRelated('Place_model', 'personId', $offset, $limit, $sortOrder, 'peopleToPlaces', 'placeId');
    }

    public function connectPlace(Place_model $place) {
        $this->load('Ptp_bridge_model');
        $bridge = new Ptp_bridge_model();
        $bridge->setPersonId($this->id);
        $bridge->setPlaceId($place->getId());
        return $bridge;
    }

    public function getPhoneNumbers($offset=0, $limit=null, $sortOrder='id') {
    /* needs done */
        $data = array();
        $limit = (($limit == null)? '18446744073709551615' : $limit);
        $c = get_called_class();

        $sql = "SELECT number, type FROM phonenumbers ORDER BY '{$sortOrder}' LIMIT {$offset} , {$limit}";

        if (($result = $database->getRows($sql)) === false)
            throw new InvalidQuery('People->getPhoneNumbers() | class/people.php', $sql);

        // Do we want to make a phoneNumber Class?

        return $data;
    }

    public function getCreatedDate($dateFormat=false) {
        if ($dateFormat !== false)
            return strftime($dateFormat, $this->createdDate);
        else
            return $this->createdDate;
    }

    public function getBirthdate($dateFormat=false) {
        if ($dateFormat !== false)
            return strftime($dateFormat, $this->birthdate);
        else
            return $this->birthdate;
    }

    public function getPictures() {
        /* yet to be done

        $sql = "SELECT * FROM picturesToPeople WHERE `personId`='{$this->id}'";

        */
    }

    public function getConnections($offset='0', $limit=null, $sortOrder='id') {
        try {
            return $this->getRelated('Ptp_bridge_model', 'personId', $offset, $limit, $sortOrder);
        } catch (InvalidQuery $e) {
            echo $e->getError();
            echo "\n<br>\n";
            echo $e->getQuery();
        }
    }

    public function getUser() {
        $this->load->model('User_model');
        if ($this->id) {
            $sql = "SELECT `id` FROM users WHERE `personId`={$this->id} LIMIT 1";
            $query = $this->db->query($sql);
            if (count($query)) {
                $query = $query->row();
                return new User_model($query->id);
            } else
                return false;
        } else
            return false;
    }

    public function save($keyNames = false) {
        if ($keyNames === false) // so that it can be overwritten, only  define if not set
            $keyNames = array();

        if (in_array('createdDate', $this->changed)) {
            $keyNames['createdDate'] = "FROM_UNIXTIME({$this->createdDate})";
        }

        if (in_array('birthdate', $this->changed)) {
            $keyNames['birthdate'] = "FROM_UNIXTIME({$this->birthdate})";
        }

        parent::save($keyNames);
    }
}

?>
