<?php
class Admin_model extends MY_Model {
    const TABLE_NAME = 'admins';

    private $id, $username, $email;

    protected
        $private = array('id', 'password');

    static
        $columnNames = array(
			'id' => 'id',
			'username' => 'username',
			'email' => 'email',
            'password' => 'password'
        );

    public function __construct($id = false) {
        if ($id !== false) {
            $sql = "SELECT 
                        id, 
                        username, 
                        email 
                    FROM admins WHERE `id`='{$id}' LIMIT 1";
            
            $query = $this->db->query($sql);
            $result = $query->row_array();

            $this->id           = $id;
            $this->username     = $result['username'];
            $this->email        = $result['email'];
        }
    }

    public static function login($username, $password) {
		$username = $this->db->escape($username);
        $sql = "SELECT id, password FROM admins WHERE `username`='{$username}' OR `email`='{$username}' LIMIT 1";

        $query = $this->db->query($sql);
        if ($query->num_rows()) {
            $this->load->library('crypt');
            $result = $query->row_array();
            $hash = $result['password'];
            if ($this->crypt->check($hash, $password)) {
                $user = new User($result['id']);
                $this->session->set_userdata(array('user' => $user));
                return true;
            }
        } else
            return false;
    }

    public static function isLoggedIn() {
        return ($this->session->userdata('user') instanceof User_model);
    }

    public static function hasPermission() {
        // checks to see if the user has permission to be on the current page,
        // or maybe something else? Lets decide later
    }

    public function logout() {
        $this->session->userdata('user');
    }

    public static function exists($username, $email=false) {
        if ($email)
            $type = 'email';
        else
            $type = 'username';

        $username = $this->db->escape($username);
        $sql = "SELECT COUNT(*) AS c FROM users WHERE `{$type}`='{$username}'";
        $query = $this->db->query($sql);
        $result = $query->rows();
        return ($result->c == 0);
    }

    public function save($password=false, $hashed=false) {
        $keyNames = array();

        if ($password !== false) {
            $this->load->library('crypt');
            $keyNames['password'] = ($hashed)? $password : $this->crypt->hash($password);
        }

        parent::save($keyNames);
    }
}
?>
