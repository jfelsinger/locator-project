<?php

class Alert_notification_model extends MY_Model {
    const TABLE_NAME = 'alertnotifications';

    protected
        $id,
        $alertId,
        $leadId;

    protected
        $private = array('id');

    static
        $columnNames = array(
            'id' => 'id',
            'alertId' => 'alertId',
            'leadId' => 'leadId'
        );

    public function __construct($id = false) {
        if ($id !== false) {
            $sql = "SELECT
                        id,
                        alertId,
                        leadId
                        FROM ".TABLE_NAME." WHERE `id`='{$id}' LIMIT 1";

            $query = $this->db->query($sql);
            $result = $query->row_array();

            $this->id       = $id;
            $this->alertId  = $result['alertId'];
            $this->leadId   = $result['leadId'];
        }
    }

    public function getAlert() {
        $this->load->model('Alert_model');
        return new Alert_model($this->alertId);
    }

    public function getLead() {
        return $this->ion_auth->user($this->leadId);
    }
?>
