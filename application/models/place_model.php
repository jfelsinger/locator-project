<?php
/**
 * public::
 * getType()                    // string : Give the placetype that is set
 * connectPerson($person)       // null : connects place to a person
 * getCreatedDate($dateFormat)  // string : returns the date the place was added
 * getConnections(
 *      $id=false,
 *      $offset='0',
 *      $limit=null,
 *      $sortOrder='id')        // array : return an array of Ptp_bridge_models
 * getPeople(
 *      $offset, 
 *      $limit=null, 
 *      $sortOrder='id')        // array : returns an array of Person_models
 *
 * static:: 
 *      getTypes()                    // array : a multi-d array with ids and names
 *      getCountries()                // array : a multi-d array of ids and names
 */
class Place_model extends MY_Model {
    const TABLE_NAME = 'places';

    protected
        $id,
        $name,
        $address1,
        $address2,
        $city,
        $region,
        $zipcode,
        $countryId,
        $country,
        $placeType,
        $placeTypeId,
        $createdDate,
        $description,
        $otherText;

    protected
        $private = array('id');

    static
        $columnNames = array(
            'id' => 'id',
            'name' => 'name',
            'address1' => 'address1',
            'address2' => 'address2',
			'city' => 'city',
			'region' => 'region',
			'zipcode' => 'zipcode',
			'countryId' => 'countryId',
			'placeTypeId' => 'placeTypeId',
            'createdDate' => 'createdDate',
            'description' => 'description',
			'otherText' => 'otherText',
        );


    function __construct($id=false) {
        if ($id !== false) {
            $sql = "SELECT
                        p.id,
                        p.name,
                        p.address1,
                        p.address2,
                        p.city,
                        p.region,
                        p.createdDate,
                        p.zipcode,
                        p.countryId,
                        p.otherText,
                        p.placeTypeId,
                        p.description,
                        cn.name as country,
                        pt.type as placeType
                    FROM 
                        places p
                    LEFT JOIN
                        (placetypes pt, countries c, countrynames cn)
                    ON
                        (p.placeTypeId=pt.id AND p.countryId=c.id AND c.id=cn.countryId)
                    WHERE
                        p.id='{$id}' AND cn.languageId='EN'
                    LIMIT 1";

            $query = $this->db->query($sql);
            $result = $query->row_array();

            $this->id           = $id;
            /*
            $this->name         = (isset($result['name'])) ? $result['name'] : '';
            $this->address1     = (isset($result['address1'])) ? $result['address1'] : '';
            $this->address2     = (isset($result['address2'])) ? $result['address2'] : '';
            $this->city         = (isset($result['city'])) ? $result['city'] : '';
            $this->region       = (isset($result['region'])) ? $result['region'] : '';
            $this->zipcode      = (isset($result['zipcode'])) ? $result['zipcode'] : '';
            $this->country      = (isset($result['country'])) ? $result['country'] : '';
            $this->countryId    = (isset($result['countryId'])) ? $result['countryId'] : '';
            $this->placeType    = (isset($result['placeType'])) ? $result['placeType'] : '';
            $this->placeTypeId  = (isset($result['placeTypeId'])) ? $result['placeTypeId'] : '';
            $this->description  = (isset($result['description'])) ? $result['description'] : '';
            */
            $this->name         = $result['name'];
            $this->address1     = $result['address1'];
            $this->address2     = $result['address2'];
            $this->city         = $result['city'];
            $this->region       = $result['region'];
            $this->zipcode      = $result['zipcode'];
            $this->country      = $result['country'];
            $this->countryId    = $result['countryId'];
            $this->placeType    = $result['placeType'];
            $this->placeTypeId  = $result['placeTypeId'];
            $this->description  = $result['description'];
            $this->otherText    = $result['otherText'];
            $this->otherText    = (isset($result['otherText'])) ? $result['otherText'] : '';
        }
    }

    public function getPeople($offset='0', $limit=null, $sortOrder='id') {
        // change getRelated method when the time comes
        return $this->getRelated('Person_model', 'placeId', $offset, $limit, $sortOrder, 'peopleToPlaces', 'personId');
    }

    public function getConnections($id=false, $offset='0', $limit=null, $sortOrder='id') {
        return $this->getRelated('Ptp_bridge_model', 'placeId', $offset, $limit, $sortOrder);
    }

    public function connectPerson(Person_model $person) {
        $bridge = new Ptp_Bridge_model();
        $bridge->setPersonId($person->getId());
        $bridge->setPlaceId($this->id);
        return $bridge;
    }

    public function getCreatedDate($dateFormat=false) {
        if ($dateFormat !== false)
            return strftime($dateFormat, $this->createdDate);
        else
            return $this->createdDate;
    }

    public static function getTypes() {
        $return = array();
        $sql = "SELECT id, type FROM placetypes";
        $query = static::$db->query($sql);

        foreach ($query->result_array() as $row)
            $return[] = $row;

        return $return;
    }

    public static function getCountries() {
        $return = array();
        $sql = "SELECT 
                    c.id, cn.name 
                FROM 
                    countries c
                LEFT JOIN
                    countrynames cn
                ON
                    c.id=cn.countryId
                WHERE
                    cn.languageId='EN'";

        $query = static::$db->query($sql);
        foreach ($query->result_array() as $row)
            $return[] = $row;

        return $return;
    }

    public function getCountry() {
        return $this->country;
    }

    public function getType() {
        if ($this->placeTypeId == 0) // An id that is equivalent to "other"
            return $this->otherText;
        else
            return $this->placeType;
    }
}
?>
