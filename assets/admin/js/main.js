$(document).ready(function() {

    /*checkboxes*/
    $('th input[type=checkbox]').click(function() {
        if ($(this).prop('checked'))
            $('.checkbox input').prop('checked', true);
        else
            $('.checkbox input').prop('checked', false);
    });

});

$(window).load(function() {
    /*datepicker*/
    $(".datepicker").datepicker({
        /*
        changeMonth: true,
        */
        changeYear: true,
        yearRage: "c-65:c+10",
        dateFormat: "yy-mm-dd"
    });
});
