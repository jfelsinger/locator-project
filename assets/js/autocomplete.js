// stop annoying errors from being thrown in ie
// can probably be a little less
if (!console) {
    var console = {
        log: function(text) {}
    };
}

var baseUrl = "http://localhost/";

jQuery.extend({
    postJSON: function(url, data, callback) {
        return jQuery.post(url,data,callback,"json");
    }
});

function split(val) {
    return val.split(/,\s*/);
}

function extractLast(term) {
    return split(term).pop();
}

$(document).ready(function() {
    startAutocomplete();

    $('.orphanageInfo').css('display','none');
    $('[name=personAdopted]').click(function() {
        if ($(this).val() == 1)
            $('.orphanageInfo').css('display','block');            
        else
            $('.orphanageInfo').css('display','none');
    });
    if ($('[name=personAdopted]:checked').val() == 1)
        $('.orphanageInfo').css('display', 'block');
});


function startAutocomplete() {

    //// Autocomplete for places ////
    $('.autoPlace .autocomplete').autocomplete({
        minLength: 0,
        source: function(request,response) {
            $.postJSON(baseUrl + "ajax/autocomplete/place", {
                text: extractLast(request.term),
                type: $('.autoPlace [name=type]').attr('value')
            }, response);
        },
        search: function() {
            var term = extractLast(this.value);
            if (term.length<4) {
                return false;
            }
        },
        appendTo: ".autoPlace",
        /*
        focus: function(event, ui) {
            $(this).val(ui.item.label);
            return false;
        },
        */
        select: function(event, ui) {
            $(this).val(ui.item.label);
            $(this).siblings('input.dataId').val(ui.item.id);

            $(this).parents('.autoPlace').siblings('.address1').val(ui.item.address1);
            $(this).parents('.autoPlace').siblings('.address2').val(ui.item.address2);
            $(this).parents('.autoPlace').siblings('.city').val(ui.item.city);
            $(this).parents('.autoPlace').siblings('.zipcode').val(ui.item.zipcode);
            $(this).parents('.autoPlace').siblings('.country').val(ui.item.countryId);

            return false;
        },
        change: function(event, ui) {
            if (ui.item) {
                $(this).siblings('input.dataId').val(ui.item.id);

                $(this).parents('.autoPlace').siblings('.address1').val(ui.item.address1);
                $(this).parents('.autoPlace').siblings('.address2').val(ui.item.address2);
                $(this).parents('.autoPlace').siblings('.city').val(ui.item.city);
                $(this).parents('.autoPlace').siblings('.zipcode').val(ui.item.zipcode);
                $(this).parents('.autoPlace').siblings('.country').val(ui.item.countryId);
            } else {
                $(this).siblings('input.dataId').val('');
                // create oter needed forms
                $(this).parents('.autoPlace').siblings('.address1').val('');
                $(this).parents('.autoPlace').siblings('.address2').val('');
                $(this).parents('.autoPlace').siblings('.city').val('');
                $(this).parents('.autoPlace').siblings('.zipcode').val('');
                $(this).parents('.autoPlace').siblings('.country').val(1);
            }
        }
    });
    //// Autocomplete places end ////
    
    //// Autocomplete for countries ////
    $('.autoCountry .autocomplete').autocomplete({
        minLength: 0,
        source: function(request,response) {
            $.postJSON(baseUrl + "ajax/autocomplete/country", {
                text: extractLast(request.term)
            }, response);
        },
        appendTo: ".autoCountry",
        select: function(event, ui) {
            $(this).val(ui.item.label);
            return false;
        },
        change: function(event, ui) {
            if (ui.item) {
                $(this).siblings('input.dataId').val(ui.item.id);
            } else {
                $(this).siblings('input.dataId').val('');
            }
        }
    });
    //// Autocomplete countries end ////
}
