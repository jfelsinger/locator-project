DROP TABLE IF EXISTS `groups`;

# Table structure for table 'groups'
CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
);

# Dumping data for table 'groups'
INSERT INTO `groups` (`id`, `name`, `description`) VALUES
	(1,'admin','Administrator'),
	(2,'members','General User');

# bbrown - 01-26-2012
# Alter statements to be able to remove the admins table
ALTER TABLE `adminstocases`
DROP FOREIGN KEY `adminstocases_ibfk_3`,
DROP FOREIGN KEY `adminstocases_ibfk_4`;

ALTER TABLE `alerts`
DROP FOREIGN KEY `alerts_ibfk_4`;

ALTER TABLE `comments`
DROP FOREIGN KEY `comments_ibfk_1`;

DROP TABLE `admins`;

# Constraints for table `adminstocases`
ALTER TABLE `adminstocases`
  ADD CONSTRAINT `adminstocases_ibfk_3` FOREIGN KEY (`adminId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `adminstocases_ibfk_4` FOREIGN KEY (`caseId`) REFERENCES `cases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

# Constraints for table `alerts` changing to reference `users` table instead of now defunct `admins` table
ALTER TABLE `alerts`
  ADD CONSTRAINT `alerts_ibfk_4` FOREIGN KEY (`adminId`) REFERENCES `users` (`id`);

# Constraints for table `comments` changing to reference `users` table instead of now defunct `admins` table
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`adminId`) REFERENCES `users` (`id`);


# Table structure for table 'users'
# bbrown - 01-25-2012
# Removed columns - first_name, last_name, company, phone
# Added columns - banned, personId, rating
ALTER TABLE `users`
ADD COLUMN `ip_address` varbinary(16) NOT NULL,
ADD COLUMN `salt` varchar(40) DEFAULT NULL,
ADD COLUMN `activation_code` varchar(40) DEFAULT NULL,
ADD COLUMN `forgotten_password_code` varchar(40) DEFAULT NULL,
ADD COLUMN `forgotten_password_time` int(11) unsigned DEFAULT NULL,
ADD COLUMN `remember_code` varchar(40) DEFAULT NULL,
ADD COLUMN `created_on` int(11) unsigned NOT NULL,
ADD COLUMN `last_login` int(11) unsigned DEFAULT NULL,
ADD COLUMN `active` tinyint(1) unsigned DEFAULT NULL,
ADD COLUMN `banned` tinyint(1) unsigned DEFAULT NULL,
DROP COLUMN `joinDate`,
DROP COLUMN `type`,
CHANGE COLUMN userName username varchar(55);

-- CREATE TABLE `users` (
--   `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
--   `ip_address` varbinary(16) NOT NULL,
--   `username` varchar(100) NOT NULL,
--   `password` varchar(80) NOT NULL,
--   `salt` varchar(40) DEFAULT NULL,
--   `email` varchar(100) NOT NULL,
--   `activation_code` varchar(40) DEFAULT NULL,
--   `forgotten_password_code` varchar(40) DEFAULT NULL,
--   `forgotten_password_time` int(11) unsigned DEFAULT NULL,
--   `remember_code` varchar(40) DEFAULT NULL,
--   `created_on` int(11) unsigned NOT NULL,
--   `last_login` int(11) unsigned DEFAULT NULL,
--   `active` tinyint(1) unsigned DEFAULT NULL,
--   `banned` tinyint(1) unsigned DEFAULT NULL,
--   `personId` int(11) unsigned NOT NULL,
--   `rating` int(11) unsigned NOT NULL,
--   PRIMARY KEY (`id`)
-- );

# Dumping data for table 'users'
# bbrown - 01-25-2012
# Modified this query to replace unnessacary columns with the new banned, personId, and rating columns
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `created_on`, `last_login`, `active`, `banned`, `personId`, `rating`) VALUES
	('18',0x7f000001,'administrator','59beecdf7fc966e2f17fd8f65a4a9aeb09d4a3d4','9462e8eee0','admin@admin.com','',NULL,'1268889823','1268889823','1', '0', '2', '0');


DROP TABLE IF EXISTS `users_groups`;

#
# Table structure for table 'users_groups'
#

CREATE TABLE `users_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
	(1,18,1),
	(2,18,2);


DROP TABLE IF EXISTS `login_attempts`;

#
# Table structure for table 'login_attempts'
#

CREATE TABLE `login_attempts` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
);
